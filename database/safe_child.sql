-- MySQL dump 10.13  Distrib 8.1.0, for macos13 (arm64)
--
-- Host: localhost    Database: school_child
-- ------------------------------------------------------
-- Server version	8.1.0

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_reset_tokens_table',1),(3,'2014_10_12_100000_create_password_resets_table',1),(4,'2019_08_19_000000_create_failed_jobs_table',1),(5,'2019_12_14_000001_create_personal_access_tokens_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_reset_tokens`
--

DROP TABLE IF EXISTS `password_reset_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_reset_tokens`
--

LOCK TABLES `password_reset_tokens` WRITE;
/*!40000 ALTER TABLE `password_reset_tokens` DISABLE KEYS */;
INSERT INTO `password_reset_tokens` VALUES ('admin@schoolchild.com','$2y$10$tPCJrZRyK5i2gYNS0/qjJOntH45kQ1m9A.ix5ABI.Zp5YJCG7yRlK','2023-09-25 09:10:02');
/*!40000 ALTER TABLE `password_reset_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `access_token` longtext COLLATE utf8mb4_unicode_ci,
  `user_id` int DEFAULT NULL,
  `role` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `school_id` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'nardconcept','nardconcept',NULL,'$2y$10$s9Telyv1.XYmpW7Vc5w7WeaU76xMghjZ4f0j4y/hpUd4C1ZWGFssK','xKfjcnhhsM9o2vE0312uwE1ZJSNsPQjhyStLGncsm80HO0PZSQJsyxaVW8EL','2023-09-25 08:26:53','2023-12-01 09:19:10','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiJuYXJkY29uY2VwdCIsInNjb3BlIjpbInJlYWQiXSwiZ2VuZXJhdGVkSW5ab25lIjoiQWZyaWNhL0thbXBhbGEiLCJleHAiOjE3MDE0NzYzNTAsInVzZXIiOnsiaWQiOjE4LCJ1c2VybmFtZSI6Im5hcmRjb25jZXB0IiwicGFzc3dvcmQiOm51bGwsInVzZXJHcm91cCI6eyJpZCI6MSwiY3JlYXRlZE9uIjoxNjk1OTIyNjMwODgwLCJtb2RpZmllZE9uIjpudWxsLCJuYW1lIjoiQURNSU5fR1JPVVAiLCJub3RlIjoic3lzdGVtIGFkbWluaXN0cmF0aXZlIHVzZXJzIn0sImVuYWJsZWQiOnRydWUsImF1dGhvcml0aWVzIjpbeyJhdXRob3JpdHkiOiJBRE1JTl9ST0xFLlJFQUQifSx7ImF1dGhvcml0eSI6IkFETUlOX1JPTEUuVVBEQVRFIn0seyJhdXRob3JpdHkiOiJBRE1JTl9ST0xFLkRFTEVURSJ9LHsiYXV0aG9yaXR5IjoiQURNSU5fUk9MRS5XUklURSJ9XSwiYWNjb3VudE5vbkV4cGlyZWQiOnRydWUsImFjY291bnROb25Mb2NrZWQiOnRydWUsImNyZWRlbnRpYWxzTm9uRXhwaXJlZCI6dHJ1ZSwiY29tcGFueVVzZXIiOmZhbHNlfSwiYXV0aG9yaXRpZXMiOlsiQURNSU5fUk9MRS5ERUxFVEUiLCJBRE1JTl9ST0xFLlJFQUQiLCJBRE1JTl9ST0xFLldSSVRFIiwiQURNSU5fUk9MRS5VUERBVEUiXSwianRpIjoiWWVLc0FIczhiOGtnOXZ5MnpLcXlYMEJkdkZBIiwiY2xpZW50X2lkIjoiY2xpZW50In0.bINs7DWQ2KxY_-6b8mkB53emSEsgVP3I6dQQp1WOe0Ga_NktpNHepbJgFbr2GCw4bQJLH7BYUWXcf_d1b1_YwHUAJzTBnvBwuWlLqt5ZpYEZ_xUhn6du2hJXWM7ruHSvSpREDKAf0uh-ndC6kfNIdFdFyFSsAl1bW8hJF-SGyQm2yoedPeFIuV9VWyM9xO5EMpyvHR-t1frSMNRMN2yp6JJBr-hwCZPyMADRXj0H8ld8uwTP6tm5pcq6Fxh7O1VDQHLDIfySuzTFpCyPt2wCtwc3HkUYlucKBBh6wxVf_l843wIYepLUFkv9Cr7P2v-kMIPyz9Xl4M1RNftRMjql4Q',18,'ADMIN_GROUP',NULL),(3,'628178','628178',NULL,'$2y$10$T174Kd1fvM15BDuw8/5XSua3Jw5EwfEsXc/tiePcNB//4o2zOWMFu',NULL,'2023-11-24 06:35:10','2023-11-24 06:38:22','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiI2MjgxNzgiLCJzY29wZSI6WyJyZWFkIl0sImdlbmVyYXRlZEluWm9uZSI6IkFmcmljYS9LYW1wYWxhIiwiZXhwIjoxNzAwODYxOTAyLCJ1c2VyIjp7ImlkIjoyNSwidXNlcm5hbWUiOiI2MjgxNzgiLCJwYXNzd29yZCI6bnVsbCwiZW5hYmxlZCI6dHJ1ZSwiYXV0aG9yaXRpZXMiOltdLCJhY2NvdW50Tm9uTG9ja2VkIjp0cnVlLCJjcmVkZW50aWFsc05vbkV4cGlyZWQiOnRydWUsImFjY291bnROb25FeHBpcmVkIjp0cnVlLCJjb21wYW55VXNlciI6ZmFsc2V9LCJqdGkiOiJJSXNyei0zdXpadlFRbUF1aWRhMnNJaXVacmciLCJjbGllbnRfaWQiOiJjbGllbnQifQ.gQRAzxFpwbbRTb9Njm2AZCjR3Zdj9yMXhzxF6kqzq7HDuma-b5aUiJXv5WYFd__CGecjUpjYTOpZlljiE5SSatwoU_uDWLclZKfHlJzhbmvRW3D4arYtZJw1rY-obF4GhgP9d1pbB6ilJPnp6-WIONUuSM6CO_z6VyEfhk2iaRWJuA-7qJDplYj6paPKYaZwzDFZv-JSXGzE0zZh9RuI7mYoFxr-6uPaluACiJUqY5N6Z0i-Ivpyq7AMzbiHHLbukhJm7APD1wrbAhd4YbZqwfLNVEuofC-JOxBID9FbAqd3BCO5QG8FOIvewhmhNqnaNQegJsuq8-IRq9U_ZJZKew',25,'ADMIN',NULL),(4,'614053','614053',NULL,'$2y$10$b1Q9AMMeVtvxb3ZVlG/kvu76YAGJwcKDk6PiRG.MiMkPcTHBVTWmG',NULL,'2023-11-29 04:53:43','2023-12-05 05:17:17','eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX25hbWUiOiI2MTQwNTMiLCJzY29wZSI6WyJyZWFkIl0sImdlbmVyYXRlZEluWm9uZSI6IkFmcmljYS9LYW1wYWxhIiwiZXhwIjoxNzAxODA3NDM3LCJ1c2VyIjp7ImlkIjoyNiwidXNlcm5hbWUiOiI2MTQwNTMiLCJwYXNzd29yZCI6bnVsbCwidXNlckdyb3VwIjp7ImlkIjoyLCJjcmVhdGVkT24iOjE2OTY5NDExNjIwNjYsIm1vZGlmaWVkT24iOjE2OTY5NDExNjIwNzYsIm5hbWUiOiJTQ0hPT0xBRE1JTl9HUk9VUCIsIm5vdGUiOiJTY2hvb2wgYWRtaW5pc3RyYXRvcnMsIGFuZCB3aGF0IHRoZSBjYW4gZG8ifSwiZW5hYmxlZCI6dHJ1ZSwiYXV0aG9yaXRpZXMiOlt7ImF1dGhvcml0eSI6IlZFSElDTEVfUk9MRS5VUERBVEUifSx7ImF1dGhvcml0eSI6IlRSSVBfUk9MRS5XUklURSJ9LHsiYXV0aG9yaXR5IjoiVkVISUNMRV9ST0xFLkRFTEVURSJ9LHsiYXV0aG9yaXR5IjoiVFJJUF9ST0xFLlVQREFURSJ9LHsiYXV0aG9yaXR5IjoiVVNFUl9ST0xFLldSSVRFIn0seyJhdXRob3JpdHkiOiJWRUhJQ0xFX1JPTEUuUkVBRCJ9LHsiYXV0aG9yaXR5IjoiVVNFUl9ST0xFLlJFQUQifSx7ImF1dGhvcml0eSI6IlNDSE9PTF9ST0xFLlJFQUQifSx7ImF1dGhvcml0eSI6IlRSSVBfUk9MRS5ERUxFVEUifSx7ImF1dGhvcml0eSI6IlVTRVJfUk9MRS5VUERBVEUifSx7ImF1dGhvcml0eSI6IlZFSElDTEVfUk9MRS5XUklURSJ9LHsiYXV0aG9yaXR5IjoiU1RVREVOVF9ST0xFLlJFQUQifSx7ImF1dGhvcml0eSI6IkxPQ0FUSU9OX1JPTEUuUkVBRCJ9LHsiYXV0aG9yaXR5IjoiU1RVREVOVF9ST0xFLkRFTEVURSJ9LHsiYXV0aG9yaXR5IjoiVFJJUF9ST0xFLlJFQUQifSx7ImF1dGhvcml0eSI6IlNUVURFTlRfUk9MRS5XUklURSJ9LHsiYXV0aG9yaXR5IjoiU1RVREVOVF9ST0xFLlVQREFURSJ9XSwiYWNjb3VudE5vbkV4cGlyZWQiOnRydWUsImFjY291bnROb25Mb2NrZWQiOnRydWUsImNyZWRlbnRpYWxzTm9uRXhwaXJlZCI6dHJ1ZSwiY29tcGFueVVzZXIiOmZhbHNlfSwiYXV0aG9yaXRpZXMiOlsiVkVISUNMRV9ST0xFLlVQREFURSIsIlRSSVBfUk9MRS5XUklURSIsIlZFSElDTEVfUk9MRS5ERUxFVEUiLCJUUklQX1JPTEUuVVBEQVRFIiwiVVNFUl9ST0xFLldSSVRFIiwiVkVISUNMRV9ST0xFLlJFQUQiLCJVU0VSX1JPTEUuUkVBRCIsIlNDSE9PTF9ST0xFLlJFQUQiLCJUUklQX1JPTEUuREVMRVRFIiwiVVNFUl9ST0xFLlVQREFURSIsIlZFSElDTEVfUk9MRS5XUklURSIsIlNUVURFTlRfUk9MRS5SRUFEIiwiTE9DQVRJT05fUk9MRS5SRUFEIiwiU1RVREVOVF9ST0xFLkRFTEVURSIsIlRSSVBfUk9MRS5SRUFEIiwiU1RVREVOVF9ST0xFLldSSVRFIiwiU1RVREVOVF9ST0xFLlVQREFURSJdLCJqdGkiOiJmQ3hJOEplc3pDckhlb0JRY1FnRWJnTkYzQVkiLCJjbGllbnRfaWQiOiJjbGllbnQifQ.nOdyLDe3SYwfIKzW23BZOvBoszoPJW6E7ZuErefPKVIZr_iEo5RUIHVAc_rMK6SypDgOqZfcRnegeEhw4nS_U4OrLfCwgWdn8-jF_ZsaJYF_2mdOuMbRzi_LQNphHc6xNIj9bNCPL994ScNgH7eBDQhNCnEkMozQ0-vuwGVCnbi55ErsEqmjDszEYkewmykq9bKIJwA2ksLouNQLx52OBF-QNFLum-oI0he3bigDshr0nyjsb2W4jNa97g0dPQbBbXqifd5x8cBuSJ8e0km5Q_iuhDXENmL0CNCnL93Fkzwix7gpNAUh4BTIk1vczhcTpUjZIWNr0qPNYzG8ooznog',26,'SCHOOLADMIN_GROUP','1');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-12-08 13:32:05
