<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Login;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class CurrentLogin
{
    /**
     * Handle the event.
     *
     * @param  \Illuminate\Auth\Events\Login $event
     * @return void
     */
    public function handle(Login $event)
    {
        $this->getAccessToken();
    }

    public function getAccessToken()
    {
        Log::info('Logging in...');

        $response = Http::withBasicAuth('client', 'client')
            ->asForm()
            ->post(env('ACCESS_TOKEN_URL'), [
                'username' => 'nardconcept',
                'password' => 'Nard@256',
                'grant_type' => 'password'
            ]);
        $response = json_decode($response->body());

        $token = $response->access_token;
        $userId = $response->user->id;

        $user = Auth::user();
        $user->access_token = $token;
        $user->user_id = $userId;
        $user->save();
        Log::info('User updated successfuly');
    }
}
