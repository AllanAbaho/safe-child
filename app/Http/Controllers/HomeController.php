<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/user/pageable?page=0&size=100');
        $users = json_decode($response->body());
        $schoolResponse = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/school/all?page=0&size=100');
        $schools = json_decode($schoolResponse->body());
        $approvalsResponse = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/user/approvals?page=0&size=100&status=PENDING');
        $approvals = json_decode($approvalsResponse->body());
        $studentsResponse = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/student/all-schools?page=0&size=10000');
        $students = json_decode($studentsResponse->body());

        return view('home', ['users' => count($users), 'schools' => count($schools), 'approvals' => count($approvals), 'students' => count($students)]);
    }
}
