<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Http;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);
        $details = [
            'username' => $request->username,
            'password' => $request->password,
        ];
        $response = $this->getAccessToken($request->username, $request->password);

        if ($response->ok()) {
            $response = json_decode($response->body());
        } else {
            $error = 'Invalid credentials supplied';
            return back()->with('error', $error)->withInput($details);
        }

        $token = $response->access_token;
        $userId = $response->user->id;
        $role = $response->user->userGroup->name;
        $schoolId = null;
        if ($role == 'SCHOOLADMIN_GROUP') {
            $response = Http::withToken($token)->get(env('API_URL') . '/user/school-user/' . $userId);
            $userResponse = json_decode($response->body());
            $schoolId = $userResponse->school->id;
        }


        $user = User::where('user_id', $userId)->first();
        if ($user == null) {
            $newUser = new User;
            $newUser->email = $request->username;
            $newUser->password = bcrypt($request->password);
            $newUser->name = $request->username;
            $newUser->access_token = $token;
            $newUser->user_id = $userId;
            $newUser->role = $role;
            $newUser->school_id = $schoolId;
            $newUser->save();
        } else {
            $user->password = bcrypt($request->password);
            $user->email = $request->username;
            $user->name = $request->username;
            $user->access_token = $token;
            $user->user_id = $userId;
            $user->role = $role;
            $user->school_id = $schoolId;
            $user->save();
        }

        if (Auth::attempt(['email' => $request->username, 'password' => $request->password])) {
            if ($schoolId !== null) {
                return redirect()->route('schools.dashboard', ['id' => $schoolId]);
            }
            return redirect()->route('home');
        }
    }

    public function getAccessToken($username, $password)
    {

        $response = Http::withBasicAuth('client', 'client')
            ->asForm()
            ->post(env('ACCESS_TOKEN_URL'), [
                'username' => $username,
                'password' => $password,
                'grant_type' => 'password'
            ]);
        return $response;
    }
}
