<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class SchoolsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function dashboard($id)
    {
        $studentsResponse = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/student/all?page=0&size=100&schoolId=' . $id);
        $students = json_decode($studentsResponse->body());

        $shuttlesResponse = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/shuttle?page=0&size=100&schoolId=' . $id);
        $shuttles = json_decode($shuttlesResponse->body());

        $driversResponse = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/user/school/' . $id . '?userType=DRIVER');
        $drivers = json_decode($driversResponse->body());

        $staffResponse = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/user/school/' . $id . '?userType=SCHOOL_STAFF');
        $staff = json_decode($staffResponse->body());

        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/school/' . $id);
        $school = json_decode($response->body());

        $accountResponse = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/account/account/balance');
        $accountBalance = json_decode($accountResponse->body());

        return view('schools.dashboard', ['students' => count($students), 'shuttles' => count($shuttles), 'drivers' => count($drivers), 'staff' => count($staff), 'school' => $school, 'accountBalance' => $accountBalance]);
    }

    public function index()
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/school/all?page=0&size=100');
        $schools = json_decode($response->body());
        return view('schools.index', ['schools' => $schools]);
    }

    public function getSchools()
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/school/all?page=0&size=100');
        $schools = json_decode($response->body());
        return $schools;
    }

    public function edit($id)
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/school/' . $id);
        $school = json_decode($response->body());
        $countryResponse = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/countries');
        $countries = json_decode($countryResponse->body());

        return view('schools.edit', ['id' => $id, 'school' => $school, 'countries' => $countries]);
    }

    public function create()
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/countries');
        $countries = json_decode($response->body());

        return view('schools.create', ['countries' => $countries]);
    }

    public function addCreditForm()
    {
        return view('schools.add-credit');
    }

    public function addCredit(Request $request)
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/school/' . Auth::user()->school_id);
        $school = json_decode($response->body());

        $details = [
            'phoneNumber' => $school->phoneNumber,
            // 'phoneNumber' => '256700460055',
            'amount' => $request->amount,
            'depositorName' => $school->schoolName,
            'network' => 'MTN',
        ];
        $response = Http::withToken(Auth::user()->access_token)
            ->post(env('API_URL') . '/account/initiate/momo/deposit', $details);
        if ($response->successful()) {
            $externalTransactionReference = json_decode($response->body())->externalTransactionReference;

            return redirect()->route('schools.check-transaction-status', ['id' => $externalTransactionReference, 'amount' => $request->amount])->with('success', 'Please enter PIN to process payment!');
        } else {
            $error = json_decode($response->body())->message;
            return back()->with('error', $error)->withInput($details);
        }
    }

    public function checkTransactionStatus($id, $amount)
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/account/deposit/status/' . $id);
        $status = json_decode($response->body())->transactionStatus;
        return view('schools.check-transaction', ['id' => $id, 'amount' => $amount, 'status' => $status, 'schoolId' => Auth::user()->school_id]);
    }

    public function checkPaymentStatus($id)
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/account/deposit/status/' . $id);
        $status = json_decode($response->body())->transactionStatus;
        return $status;
    }



    public function store(Request $request)
    {
        $details = [
            'schoolName' => $request->schoolName,
            'phoneNumber' => $request->phoneNumber,
            'email' => $request->email,
            'username' => 'inih',
            'physicalAddress' => $request->physicalAddress,
            'schoolCategory' => $request->schoolCategory,
            'smsCost' => (int) $request->smsCost,
            'districtId' => (int) $request->districtId,
            'countryId' => (int) $request->countryId,
            'latitudeCoordinate' => (float) $request->latitudeCoordinate,
            'longitudeCoordinate' => (float) $request->longitudeCoordinate,
        ];
        $response = Http::withToken(Auth::user()->access_token)
            ->post(env('API_URL') . '/school', $details);
        if ($response->ok()) {
            return redirect()->route('schools.index')->with('success', 'School added successfully!');
        } else {
            $error = json_decode($response->body())->message;
            return back()->with('error', $error)->withInput($details);
        }
    }

    public function update(Request $request, $id)
    {
        $response = Http::withToken(Auth::user()->access_token)
            ->put(env('API_URL') . '/school/' . $id, [
                'schoolName' => $request->schoolName,
                'phoneNumber' => $request->phoneNumber,
                'email' => $request->email,
                'username' => '',
                'physicalAddress' => $request->physicalAddress,
                'schoolCategory' => $request->schoolCategory,
                'smsCost' => (int) $request->smsCost,
                'districtId' => (int) $request->districtId,
                'countryId' => (int) $request->countryId,
                'latitudeCoordinate' => (float) $request->latitudeCoordinate,
                'longitudeCoordinate' => (float) $request->longitudeCoordinate,
            ]);
        if ($response->ok()) {
            return redirect()->route('schools.index')->with('success', 'School updated successfully!');
        } else {
            $error = json_decode($response->body())->message;
            return back()->with('error', $error);
        }
    }

    public function getDistricts($countryId)
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/districts?page=0&size=200&countryId=' . $countryId);
        $districts = json_decode($response->body());
        return $districts;
    }

    public function delete()
    {
        return redirect()->route('schools.index')->with('success', 'User deleted successfully!');
    }
}
