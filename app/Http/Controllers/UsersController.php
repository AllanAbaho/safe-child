<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function editPassword()
    {

        return view('users.edit-password');
    }
    public function changeLogo()
    {

        return view('users.change-logo');
    }
    public function postLogo(Request $request)
    {

        $user = User::where('id', Auth::id())->first();

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $extenstion = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extenstion;
            $file->move('uploads/', $filename);
            $user->logo = $filename;
        }
        $user->save();
        if ($user->school_id) {
            return redirect()->route('schools.dashboard', $user->school_id)->with('success', 'Logo uploaded successfully!');
        } else {
            return redirect()->route('home', $user->school_id)->with('success', 'Logo uploaded successfully!');
        }
    }
    public function updatePassword(Request $request)
    {
        $details = [
            'oldPassword' => $request->get('oldPassword'),
            'newPassword' => $request->get('newPassword'),
            'confirmPassword' => $request->get('confirmPassword'),
        ];
        if ($request->get('newPassword') != $request->get('confirmPassword')) {
            return back()->with('error', 'New password is not equal to confirm password')->withInput($details);
        }
        $response = Http::withToken(Auth::user()->access_token)->post(env('API_URL') . '/user/change-password', $details);

        if ($response->body() == 'true') {
            Auth::logout();
            return redirect()->route('login')->with('success', 'User password changed successfully!');
        } else {
            $error = 'Password could not be changed';
            return back()->with('error', $error)->withInput($details);
        }
    }

    public function index()
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/user/pageable?page=0&size=100');
        $users = json_decode($response->body());
        return view('users.index', ['users' => $users]);
    }

    public function approvals()
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/user/approvals?page=0&size=100&status=PENDING');
        $users = json_decode($response->body());
        // dd($users);
        return view('users.approvals', ['users' => $users]);
    }


    public function edit($id)
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/user/' . $id);
        $user = json_decode($response->body());
        return view('users.edit', ['id' => $id, 'user' => $user]);
    }
    public function approve($id, $userId)
    {
        $details = [
            'id' => (int) $id,
            'userId' => (int) $userId,
            'status' => 'APPROVED',
        ];
        $response = Http::withToken(Auth::user()->access_token)->post(env('API_URL') . '/user/approve', $details);
        if ($response->successful()) {
            return redirect()->route('users.approvals')->with('success', 'User approved successfully!');
        } else {
            $error = json_decode($response->body())->message;
            return back()->with('error', $error);
        }
    }

    public function create()
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/groups');
        $groups = json_decode($response->body());
        return view('users.create', ['groups' => $groups]);
    }
    public function store(Request $request)
    {
        $details = [
            'username' => $request->username,
            'password' => $request->password,
            'userGroupId' => (int) $request->userGroupId,
            'userType' => $request->userType,
        ];
        $response = Http::withToken(Auth::user()->access_token)
            ->post(env('API_URL') . '/user', $details);
        if ($response->ok()) {
            return redirect()->route('users.index')->with('success', 'User added successfully!');
        } else {
            $error = json_decode($response->body())->message;
            return back()->with('error', $error)->withInput($details);
        }
    }
    public function update(Request $request, $id)
    {
        $updateFields = [
            'username' => $request->username,
            // 'password' => $request->password,
            'userGroupId' => (int) $request->userGroupId,
            'userType' => $request->userType,
            'approved' => $request->approved == 'on' ? true : false,
        ];
        // dd($updateFields);
        $response = Http::withToken(Auth::user()->access_token)
            ->patch(env('API_URL') . '/user/' . $id, $updateFields);
        if ($response->ok()) {
            return redirect()->route('users.index')->with('success', 'User updated successfully!');
        } else {
            $error = json_decode($response->body())->message;
            return back()->with('error', $error);
        }
    }

    public function delete()
    {
        return redirect()->route('users.index')->with('success', 'User deleted successfully!');
    }
}
