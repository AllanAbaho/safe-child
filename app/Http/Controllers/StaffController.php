<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class StaffController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function search()
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/school/all?page=0&size=100');
        $schools = json_decode($response->body());
        return view('staff.search', ['schools' => $schools]);
    }
    public function searchResults(Request $request)
    {
        $schoolId = $request->schoolId;
        $userType = $request->userType;
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/user/school/' . $schoolId . '?userType=' . $userType);
        if ($response->ok()) {
            $users = json_decode($response->body());
            return view('staff.index', ['users' => $users, 'schoolId' => $schoolId]);
        } else {
            $error = json_decode($response->body())->message;
            return back()->with('error', $error);
        }
    }
    public function index($schoolId, $userType)
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/user/school/' . $schoolId . '?userType=' . $userType);
        $users = json_decode($response->body());
        return view('staff.index', ['users' => $users, 'schoolId' => $schoolId]);
    }

    public function getTrips($username)
    {
        $startDate = date('Y-m-d H:m:s', strtotime('-1 year'));
        $endDate = date('Y-m-d H:m:s');
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/trip/driver?page=0&size=20&username=' . $username . '&fromDate=' . $startDate . '&toDate=' . $endDate);
        $trips = json_decode($response->body());
        // dd($trips);
        return view('trips.index', ['trips' => $trips]);
    }

    public function getTripCoordinates($id, $schoolId)
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/trip/' . $id . '/event-coordinates');
        $stops = json_decode($response->body());

        $schoolResponse = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/school/' . $schoolId);
        $school = json_decode($schoolResponse->body());

        $locations = [];
        $schoolLocation = [$school->location->latitude, $school->location->longitude];
        foreach ($stops as $stop) {
            array_push($locations, [$stop->location->latitude, $stop->location->longitude]);
        }
        return view('trips.map', ['locations' => $locations, 'schoolLocation' => $schoolLocation]);
    }

    public function create($schoolId)
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/groups');
        $groups = json_decode($response->body());
        $countriesResponse = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/countries');
        $countries = json_decode($countriesResponse->body());
        return view('staff.create', ['groups' => $groups, 'countries' => $countries, 'schoolId' => $schoolId]);
    }

    public function edit($id)
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/user/school-user/' . $id);
        $user = json_decode($response->body());
        $responseMeta = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/user/meta-data/' . $user->userMetaId);
        $staff = json_decode($responseMeta->body());

        $groupsResponse = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/groups');
        $groups = json_decode($groupsResponse->body());

        $countriesResponse = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/countries');
        $countries = json_decode($countriesResponse->body());

        return view('staff.edit', ['groups' => $groups, 'countries' => $countries, 'staff' => $staff, 'user' => $user]);
    }

    public function store(Request $request)
    {
        $details = [
            'firstName' => $request->firstName,
            'lastName' => $request->lastName,
            'middleName' => $request->middleName,
            'phoneNumber' => $request->phoneNumber,
            'phoneNumber2' => $request->phoneNumber2,
            'gender' => $request->gender,
            'birthDate' => $request->birthDate,
            'email' => $request->email,
            'countryId' => (int) $request->countryId,
            'identificationType' => $request->identificationType,
            'identificationNumber' => $request->identificationNumber,
            'pin' => $request->pin,
            'password' => $request->password,
            'userGroupId' => (int) $request->userGroupId,
            'userType' => $request->userType,
            'approvedBy' => (int) Auth::user()->user_id,
            'schoolId' => (int) $request->schoolId,

        ];
        $response = Http::withToken(Auth::user()->access_token)
            ->post(env('API_URL') . '/user/school', $details);
        if ($response->ok()) {
            return redirect()->route('staff.search')->with('success', 'Staff added successfully!');
        } else {
            $error = json_decode($response->body())->message;
            return back()->with('error', $error)->withInput($details);
        }
    }
    public function update(Request $request, $id)
    {
        $details = [
            'firstName' => $request->firstName,
            'lastName' => $request->lastName,
            'middleName' => $request->middleName,
            'phoneNumber' => $request->phoneNumber,
            'phoneNumber2' => $request->phoneNumber2,
            'gender' => $request->gender,
            'birthDate' => $request->birthDate,
            'email' => $request->email,
            'countryId' => (int) $request->countryId,
            'identificationType' => $request->identificationType,
            'identificationNumber' => $request->identificationNumber,
            'userGroupId' => (int) $request->userGroupId,
            'userType' => $request->userType,
            'approvedBy' => (int) Auth::user()->user_id,
            'schoolId' => (int) $request->schoolId,

        ];
        $response = Http::withToken(Auth::user()->access_token)
            ->put(env('API_URL') . '/user/school/' . $id, $details);
        if ($response->ok()) {
            return redirect()->route('staff.index', ['id' => $id, 'userType' => $request->userType])->with('success', 'Staff updated successfully!');
        } else {
            $error = json_decode($response->body())->message;
            return back()->with('error', $error)->withInput($details);
        }
    }
}
