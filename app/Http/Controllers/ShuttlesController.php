<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;

class ShuttlesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function search()
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/school/all?page=0&size=100');
        $schools = json_decode($response->body());
        return view('shuttles.search', ['schools' => $schools]);
    }
    public function index($id)
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/shuttle?page=0&size=100&schoolId=' . $id);
        $shuttles = json_decode($response->body());
        return view('shuttles.index', ['shuttles' => $shuttles, 'id' => $id]);
    }
    public function searchResults(Request $request)
    {
        $schoolId = $request->schoolId;
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/shuttle?page=0&size=100&schoolId=' . $schoolId);
        if ($response->ok()) {
            $shuttles = json_decode($response->body());
            return view('shuttles.index', ['shuttles' => $shuttles, 'id' => $schoolId]);
        } else {
            $error = json_decode($response->body())->message;
            return back()->with('error', $error);
        }
    }

    public function edit($id)
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/shuttle/' . $id);
        $shuttle = json_decode($response->body());
        $driverResponse = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/user/school/' . $shuttle->school->id . '?userType=DRIVER');
        $drivers = json_decode($driverResponse->body());
        return view('shuttles.edit', ['id' => $id, 'shuttle' => $shuttle, 'drivers' => $drivers]);
    }
    public function create($id)
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/user/school/' . $id . '?userType=DRIVER');
        $drivers = json_decode($response->body());
        return view('shuttles.create', ['id' => $id, 'drivers' => $drivers]);
    }
    public function store(Request $request)
    {
        $details = [
            'currentDriverId' => (int) $request->currentDriverId,
            'plateNumber' => $request->plateNumber,
            'vehicleModel' => $request->vehicleModel,
            'schoolId' => (int) $request->schoolId,
            'maximumCapacity' => (int) $request->maximumCapacity,
        ];
        $response = Http::withToken(Auth::user()->access_token)
            ->post(env('API_URL') . '/shuttle', $details);
        if ($response->ok()) {
            return redirect()->route('shuttles.index', ['id' => $request->schoolId])->with('success', 'Shuttle added successfully!');
        } else {
            $error = json_decode($response->body())->message;
            return back()->with('error', $error)->withInput($details);
        }
    }
    public function update(Request $request, $id)
    {
        $response = Http::withToken(Auth::user()->access_token)
            ->put(env('API_URL') . '/shuttle/' . $id, [
                'currentDriverId' => (int) $request->currentDriverId,
                'plateNumber' => $request->plateNumber,
                'vehicleModel' => $request->vehicleModel,
                'schoolId' => (int) $request->schoolId,
                'maximumCapacity' => (int) $request->maximumCapacity,
            ]);
        if ($response->ok()) {
            return redirect()->route('shuttles.index', ['id' => $request->schoolId])->with('success', 'Shuttle updated successfully!');
        } else {
            $error = json_decode($response->body())->message;
            return back()->with('error', $error);
        }
    }

    public function delete()
    {
        return redirect()->route('users.index')->with('success', 'User deleted successfully!');
    }
}
