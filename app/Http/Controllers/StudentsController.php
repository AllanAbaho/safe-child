<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class StudentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id)
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/student/all?page=0&size=100&schoolId=' . $id);
        $students = json_decode($response->body());
        return view('students.index', ['students' => $students, 'id' => $id]);
    }
    public function all()
    {
        $studentsResponse = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/student/all-schools?page=0&size=10000');
        $students = json_decode($studentsResponse->body());
        return view('students.all', ['students' => $students]);
    }
    public function searchResults(Request $request)
    {
        $studentIdNumber = $request->studentIdNumber;
        $schoolId = $request->schoolId;
        $details = [
            'studentIdNumber' => $studentIdNumber,
            'schoolId' => $schoolId,
        ];
        if (isset($studentIdNumber)) {
            $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/student?studentIdNumber=' . $details['studentIdNumber'] . '&SchoolId=' . $details['schoolId']);
        } else {
            $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/student/all?page=0&size=10&schoolId=' . $details['schoolId']);
        }
        if ($response->ok()) {
            if (isset($studentIdNumber)) {
                $student = json_decode($response->body());
                return view('students.search-results', ['student' => $student]);
            } else {
                $students = json_decode($response->body());
                return view('students.index', ['students' => $students, 'id' => $details['schoolId']]);
            }
        } else {
            $error = json_decode($response->body())->message;
            return back()->with('error', $error)->withInput($details);
        }
    }

    public function edit($id)
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/student/' . $id);
        $student = json_decode($response->body());
        $guardianResponse = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/student/' . $id . '/guardian');
        $guadians = json_decode($guardianResponse->body());
        return view('students.edit', ['id' => $id, 'student' => $student, 'guardians' => $guadians]);
    }
    public function create($id)
    {
        return view('students.create', ['id' => $id]);
    }
    public function search()
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/school/all?page=0&size=100');
        $schools = json_decode($response->body());
        return view('students.search', ['schools' => $schools]);
    }
    public function generateCode($firstName, $schoolIdNumber)
    {
        $codeName = $schoolIdNumber . '-' . $firstName . '.png';
        return QrCode::size(300)->format('png')->generate($codeName, public_path('codes/' . $codeName));
    }
    public function store(Request $request)
    {
        $details = [
            'firstName' => $request->firstName,
            'secondName' => $request->secondName,
            'email' => $request->email,
            'schoolIdNumber' => $request->schoolIdNumber,
            'nationalIdNumber' => $request->nationalIdNumber,
            'studentClass' => $request->studentClass,
            'schoolId' => (int) $request->schoolId,
            'canBeNotified' => boolval($request->canBeNotified),
            'physicalAddress' => $request->physicalAddress,
            'qrCodeString' => $request->schoolIdNumber . '-' . $request->firstName . '.png',
        ];
        $response = Http::withToken(Auth::user()->access_token)
            ->post(env('API_URL') . '/student', $details);
        if ($response->ok()) {
            $this->generateCode($request->firstName, $request->schoolIdNumber);
            return redirect()->route('students.index', ['id' => $request->schoolId])->with('success', 'Student added successfully!');
        } else {
            $error = json_decode($response->body())->message;
            return back()->with('error', $error)->withInput($details);
        }
    }
    public function createGuardian($id)
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/student/' . $id);
        $student = json_decode($response->body());

        return view('students.create-guardian', ['id' => $id, 'student' => $student]);
    }
    public function editGuardian($studentId, $guardianId)
    {
        $response = Http::withToken(Auth::user()->access_token)->get(env('API_URL') . '/student/guardian-id/' . $guardianId);
        $guardian = json_decode($response->body());

        return view('students.edit-guardian', ['guardian' => $guardian, 'studentId' => $studentId]);
    }

    public function updateGuardian(Request $request, $studentId, $guardianId)
    {
        $details = [
            'fullName' => $request->fullName,
            'phoneNumber' => $request->phoneNumber,
            'address' => $request->address,
            'relation' => $request->relation,
            'identificationType' => $request->identificationType,
            'idNumber' => $request->idNumber,
            'isNotified' => boolval($request->isNotified),
        ];
        $response = Http::withToken(Auth::user()->access_token)
            ->patch(env('API_URL') . '/student/guardian?studentId=' . $studentId . '&guardianId=' . $guardianId, $details);
        dd($response);
        if ($response->ok()) {
            return redirect()->route('students.edit', ['id' => $request->studentId])->with('success', 'Guardian updated successfully!');
        } else {
            $error = json_decode($response->body())->message;
            return back()->with('error', $error)->withInput($details);
        }
    }

    public function guardian(Request $request)
    {
        $details = [
            'fullName' => $request->fullName,
            'phoneNumber' => $request->phoneNumber,
            'address' => $request->address,
            'relation' => $request->relation,
            'identificationType' => $request->identificationType,
            'idNumber' => $request->idNumber,
            'studentId' => (int) $request->studentId,
            'isNotified' => boolval($request->isNotified),
        ];
        $response = Http::withToken(Auth::user()->access_token)
            ->post(env('API_URL') . '/student/guardian', $details);
        if ($response->ok()) {
            return redirect()->route('students.edit', ['id' => $request->studentId])->with('success', 'Guardian added successfully!');
        } else {
            $error = json_decode($response->body())->message;
            return back()->with('error', $error)->withInput($details);
        }
    }
    public function update(Request $request, $id)
    {
        $response = Http::withToken(Auth::user()->access_token)
            ->put(env('API_URL') . '/student/' . $id, [
                'firstName' => $request->firstName,
                'secondName' => $request->secondName,
                'email' => $request->email,
                'schoolIdNumber' => $request->schoolIdNumber,
                'nationalIdNumber' => $request->nationalIdNumber,
                'studentClass' => $request->studentClass,
                'schoolId' => (int) $request->schoolId,
                'canBeNotified' => boolval($request->canBeNotified),
                'physicalAddress' => $request->physicalAddress,
            ]);
        if ($response->ok()) {
            return redirect()->route('students.index', ['id' => $request->schoolId])->with('success', 'Student updated successfully!');
        } else {
            $error = json_decode($response->body())->message;
            return back()->with('error', $error);
        }
    }

    public function delete()
    {
        return redirect()->route('users.index')->with('success', 'User deleted successfully!');
    }
}
