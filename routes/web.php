<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/users/index', [App\Http\Controllers\UsersController::class, 'index'])->name('users.index');
Route::get('/users/create', [App\Http\Controllers\UsersController::class, 'create'])->name('users.create');
Route::post('/users/store', [App\Http\Controllers\UsersController::class, 'store'])->name('users.store');
Route::post('/users/update/{id}', [App\Http\Controllers\UsersController::class, 'update'])->name('users.update');
Route::get('/users/edit/{id}', [App\Http\Controllers\UsersController::class, 'edit'])->name('users.edit');
Route::post('/users/delete', [App\Http\Controllers\UsersController::class, 'delete'])->name('users.delete');
Route::get('/users/approvals', [App\Http\Controllers\UsersController::class, 'approvals'])->name('users.approvals');
Route::get('/users/edit-password/{id}', [App\Http\Controllers\UsersController::class, 'editPassword'])->name('users.edit-password');
Route::post('/users/update-password', [App\Http\Controllers\UsersController::class, 'updatePassword'])->name('users.update-password');
Route::post('/users/approve/{id}/{userId}', [App\Http\Controllers\UsersController::class, 'approve'])->name('users.approve');
Route::get('/users/change-logo', [App\Http\Controllers\UsersController::class, 'changeLogo'])->name('users.change-logo');
Route::post('/users/post-logo', [App\Http\Controllers\UsersController::class, 'postLogo'])->name('users.post-logo');

Route::get('/schools/index', [App\Http\Controllers\SchoolsController::class, 'index'])->name('schools.index');
Route::get('/getSchools', [App\Http\Controllers\SchoolsController::class, 'getSchools'])->name('schools.getSchools');
Route::get('/schools/create', [App\Http\Controllers\SchoolsController::class, 'create'])->name('schools.create');
Route::post('/schools/store', [App\Http\Controllers\SchoolsController::class, 'store'])->name('schools.store');
Route::post('/schools/update/{id}', [App\Http\Controllers\SchoolsController::class, 'update'])->name('schools.update');
Route::get('/schools/edit/{id}', [App\Http\Controllers\SchoolsController::class, 'edit'])->name('schools.edit');
Route::get('/getDistricts/{countryId}', [App\Http\Controllers\SchoolsController::class, 'getDistricts'])->name('schools.getDistricts');
Route::post('/schools/delete', [App\Http\Controllers\SchoolsController::class, 'delete'])->name('schools.delete');
Route::get('/schools/dashboard/{id}', [App\Http\Controllers\SchoolsController::class, 'dashboard'])->name('schools.dashboard');
Route::get('/schools/add-credit-form', [App\Http\Controllers\SchoolsController::class, 'addCreditForm'])->name('schools.add-credit-form');
Route::post('/schools/add-credit', [App\Http\Controllers\SchoolsController::class, 'addCredit'])->name('schools.add-credit');
Route::get('/schools/check-transaction-status/{id}/{amount}', [App\Http\Controllers\SchoolsController::class, 'checkTransactionStatus'])->name('schools.check-transaction-status');
Route::post('/schools/check-payment-status/{id}', [App\Http\Controllers\SchoolsController::class, 'checkPaymentStatus'])->name('schools.check-payment-status');

Route::get('/students/index/{id}', [App\Http\Controllers\StudentsController::class, 'index'])->name('students.index');
Route::get('/students/all', [App\Http\Controllers\StudentsController::class, 'all'])->name('students.all');
Route::get('/students/create/{id}', [App\Http\Controllers\StudentsController::class, 'create'])->name('students.create');
Route::post('/students/store', [App\Http\Controllers\StudentsController::class, 'store'])->name('students.store');
Route::post('/students/update/{id}', [App\Http\Controllers\StudentsController::class, 'update'])->name('students.update');
Route::get('/students/edit/{id}', [App\Http\Controllers\StudentsController::class, 'edit'])->name('students.edit');
Route::post('/students/delete', [App\Http\Controllers\StudentsController::class, 'delete'])->name('students.delete');
Route::get('/students/search', [App\Http\Controllers\StudentsController::class, 'search'])->name('students.search');
Route::post('/students/search-results', [App\Http\Controllers\StudentsController::class, 'searchResults'])->name('students.search-results');
Route::post('/students/guardian', [App\Http\Controllers\StudentsController::class, 'guardian'])->name('students.guardian');
Route::get('/students/create-guardian/{id}', [App\Http\Controllers\StudentsController::class, 'createGuardian'])->name('students.create-guardian');
Route::get('/students/edit-guardian/{studentId}/{guardianId}', [App\Http\Controllers\StudentsController::class, 'editGuardian'])->name('students.edit-guardian');
Route::post('/students/update-guardian/{studentId}/{guardianId}', [App\Http\Controllers\StudentsController::class, 'updateGuardian'])->name('students.update-guardian');

Route::get('/shuttles/index/{id}', [App\Http\Controllers\ShuttlesController::class, 'index'])->name('shuttles.index');
Route::get('/shuttles/search', [App\Http\Controllers\ShuttlesController::class, 'search'])->name('shuttles.search');
Route::post('/shuttles/search-results', [App\Http\Controllers\ShuttlesController::class, 'searchResults'])->name('shuttles.search-results');
Route::get('/shuttles/create/{id}', [App\Http\Controllers\ShuttlesController::class, 'create'])->name('shuttles.create');
Route::post('/shuttles/store', [App\Http\Controllers\ShuttlesController::class, 'store'])->name('shuttles.store');
Route::post('/shuttles/update/{id}', [App\Http\Controllers\ShuttlesController::class, 'update'])->name('shuttles.update');
Route::get('/shuttles/edit/{id}', [App\Http\Controllers\ShuttlesController::class, 'edit'])->name('shuttles.edit');
Route::post('/shuttles/delete', [App\Http\Controllers\ShuttlesController::class, 'delete'])->name('shuttles.delete');

Route::get('/staff/search', [App\Http\Controllers\StaffController::class, 'search'])->name('staff.search');
Route::get('/staff/index/{schoolId}/{userType}', [App\Http\Controllers\StaffController::class, 'index'])->name('staff.index');
Route::post('/staff/store', [App\Http\Controllers\StaffController::class, 'store'])->name('staff.store');
Route::get('/staff/edit/{id}', [App\Http\Controllers\StaffController::class, 'edit'])->name('staff.edit');
Route::post('/staff/update/{id}', [App\Http\Controllers\StaffController::class, 'update'])->name('staff.update');
Route::get('/staff/create/{schoolId}', [App\Http\Controllers\StaffController::class, 'create'])->name('staff.create');
Route::get('/staff/get-trips/{username}', [App\Http\Controllers\StaffController::class, 'getTrips'])->name('staff.get-trips');
Route::post('/staff/search-results', [App\Http\Controllers\StaffController::class, 'searchResults'])->name('staff.search-results');

Route::get('/trip/coordinates/{id}/{schoolId}', [App\Http\Controllers\StaffController::class, 'getTripCoordinates'])->name('trip.coordinates');
