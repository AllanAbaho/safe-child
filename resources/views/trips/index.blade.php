@extends('layouts.dashboard')

@section('content')
    @if (session('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif
    <div class="card">

        <h5 class="card-header">List Of Trips</h5>
        <div class="table-responsive text-nowrap container mb-3">
            <table id="myTable" class="table">
                @if (count($trips) > 0)
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Trip Type</th>
                            <th>Trip Status</th>
                            <th>Trip Date</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody class="table-border-bottom-0">
                        @foreach ($trips as $trip)
                            <tr>
                                <td>{{ $trip->id }}</td>
                                <td>{{ $trip->tripType }}</td>
                                <td><span
                                        class="badge bg-label-{{ $trip->tripStatus == 'OPEN' ? 'success' : 'danger' }} me-1">{{ $trip->tripStatus }}</span>
                                <td>{{ $trip->createdOn }}</td>
                                <td><a class="btn btn-info btn-sm"
                                        href="{{ route('trip.coordinates', [$trip->id, $trip->driver->school->id]) }}">Details</a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                @else
                    <p class="text-center">No trips found</p>
                @endif
            </table>
            <br><br><br>

        </div>
    </div>
@endsection
