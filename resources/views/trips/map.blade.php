@extends('layouts.dashboard')

@section('content')
    @if (session('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif
    <div class="card">

        <h5 class="card-header">Trip Map</h5>
        <div class="card-body">
            <div id="map" style="height: 450px;
                width: 100%;
                background-color: grey;">
            </div>
            <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQLRldjFTWHYol_PtdnMv7fen804Y5s7k&callback=drawMap"></script>
        </div>
    </div>
@endsection
<script type="text/javascript">
    let directionsService, directionsRenderer;
    let locations = "<?= json_encode($locations) ?>";
    let schoolLocation = "<?= json_encode($schoolLocation) ?>";
    locations = JSON.parse(locations);
    schoolLocation = JSON.parse(schoolLocation);
    console.log(schoolLocation)
    console.log(locations)


    function drawMap() {
        var center = {
            lat: schoolLocation[0],
            lng: schoolLocation[1]
        };
        directionsService = new google.maps.DirectionsService();
        directionsRenderer = new google.maps.DirectionsRenderer();

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 20,
            center: center
        });
        google.maps.event.addListener(map, "click", function(event) {
            this.setOptions({
                scrollwheel: true,
            });
        })

        directionsRenderer.setMap(map);
        calculateRoute()
    }

    function calculateRoute() {
        let stops = []
        for (i in locations) {
            stops.push({
                location: new google.maps.LatLng(locations[i][0], locations[i][1]),
                stopover: true
            });
        }
        var request = {
            origin: new google.maps.LatLng(0.3520292286975031, 32.59250294217443),
            destination: new google.maps.LatLng(0.30387098655247, 32.55219892457992),
            waypoints: stops,
            optimizeWaypoints: true,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };

        directionsService.route(request, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsRenderer.setDirections(response);
            }
        });
    }
</script>
