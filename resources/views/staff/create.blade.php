@extends('layouts.dashboard')

@section('content')
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="card mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="mb-0">Add Staff</h5>
            <small class="text-muted float-end">Add Staff</small>
        </div>
        <div class="card-body">
            <form action="{{ route('staff.store', $schoolId) }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">First Name</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" name="firstName" class="form-control" id="basic-icon-default-fullname"
                                    placeholder="John Doe" aria-label="John" aria-describedby="basic-icon-default-fullname2"
                                    value="{{ old('firstName') }}" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Last Name</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" name="lastName" class="form-control" id="basic-icon-default-fullname"
                                    placeholder="John Doe" aria-label="John" aria-describedby="basic-icon-default-fullname2"
                                    value="{{ old('lastName') }}" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Middle Name</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" name="middleName" class="form-control"
                                    id="basic-icon-default-fullname" placeholder="John Doe" aria-label="John"
                                    aria-describedby="basic-icon-default-fullname2" value="{{ old('middleName') }}">
                            </div>
                        </div>

                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">Phone Number</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input type="text" name="phoneNumber" id="basic-icon-default-company"
                                    class="form-control" placeholder="0700568784" aria-label="Doe"
                                    aria-describedby="basic-icon-default-company2" value="{{ old('phoneNumber') }}"
                                    required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">Phone Number 2</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input type="text" name="phoneNumber2" id="basic-icon-default-company"
                                    class="form-control" placeholder="0700568784" aria-label="Doe"
                                    aria-describedby="basic-icon-default-company2" value="{{ old('phoneNumber2') }}">
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Email</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="email" name="email" class="form-control" id="basic-icon-default-fullname"
                                    placeholder="129783" aria-label="Kampala"
                                    aria-describedby="basic-icon-default-fullname2" value="{{ old('email') }}" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">Birth Date</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input class="form-control" name="birthDate" type="date"
                                    value="{{ old('birthDate') }}" id="html5-date-input" required>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">Gender</label>
                            <select id="defaultSelect" class="form-select" name="gender" required>
                                <option value="">Please Select Option</option>
                                <option value="MALE" @selected(old('gender') == 'MALE')>MALE</option>
                                <option value="FEMALE" @selected(old('gender') == 'FEMALE')>FEMALE</option>
                            </select>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">PIN</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input type="password" name="pin" id="basic-icon-default-company"
                                    class="form-control" placeholder="0700568784" aria-label="Doe"
                                    aria-describedby="basic-icon-default-company2" value="{{ old('pin') }}" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">Password</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input type="password" name="password" id="basic-icon-default-company"
                                    class="form-control" placeholder="0700568784" aria-label="Doe"
                                    aria-describedby="basic-icon-default-company2" value="{{ old('password') }}"
                                    required>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">Country</label>
                            <select id="countrySelect" class="form-select" name="countryId" required>
                                <option value="">Select Country</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}" @selected(old('countryId') == $country->id)>{{ $country->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">User Group</label>
                            <select id="countrySelect" class="form-select" name="userGroupId" required>
                                <option value="">Select Group</option>
                                @foreach ($groups as $group)
                                    <option value="{{ $group->id }}" @selected(old('userGroupId') == $group->id)>{{ $group->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">ID TYPE</label>
                            <select id="defaultSelect" class="form-select" name="identificationType" required>
                                <option value="">Please Select Identification</option>
                                <option value="NATIONAL_ID" @selected(old('identificationType') == 'NATIONAL_ID')>NATIONAL ID</option>
                                <option value="DRIVING_PERMIT" @selected(old('identificationType') == 'DRIVING_PERMIT')>DRIVING PERMIT</option>
                                <option value="PASSPORT" @selected(old('identificationType') == 'PASSPORT')>PASSPORT</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">ID Number</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input type="text" name="identificationNumber" id="basic-icon-default-company"
                                    class="form-control" placeholder="Doe" aria-label="Doe"
                                    aria-describedby="basic-icon-default-company2"
                                    value="{{ old('identificationNumber') }}" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">User Type</label>
                            <select id="defaultSelect" class="form-select" name="userType" required>
                                <option value="">Please Select User Type</option>
                                <option value="DRIVER" @selected(old('userType') == 'DRIVER')>DRIVER</option>
                                <option value="SCHOOL_ADMIN" @selected(old('userType') == 'SCHOOL_ADMIN')>SCHOOL ADMIN</option>
                                <option value="SCHOOL_STAFF" @selected(old('userType') == 'SCHOOL_STAFF')>SCHOOL STAFF</option>
                            </select>
                        </div>

                        <input type="hidden" name="schoolId" value="{{ $schoolId }}">
                    </div>
                </div>
                <button type="submit" class="btn btn-info">Add Staff</button>
            </form>
        </div>
    </div>
@endsection
