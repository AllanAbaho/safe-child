@extends('layouts.dashboard')

@section('content')
    @if (session('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif
    <div class="card">
        <div class="row">
            <div class="col-md-3">
                <a href="{{ route('staff.create', $schoolId) }}" class="btn btn-success mt-3 mx-4"> Add Staff</a>
            </div>
        </div>
        <h5 class="card-header">List Of School Staff</h5>
        <div class="table-responsive text-nowrap container mb-3">
            <table id="myTable" class="table">
                @if (count($users) > 0)
                    <thead>
                        <tr>
                            <th>Username</th>
                            <th>User Type</th>
                            <th>User Group</th>
                            <th>Approved</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody class="table-border-bottom-0">
                        @foreach ($users as $user)
                            <tr>
                                <td><i class="fab fa-angular fa-lg text-danger me-3"></i>
                                    <a
                                        href="{{ route('staff.get-trips', $user->username) }}"><strong>{{ $user->username }}</strong></a>
                                </td>
                                <td>{{ $user->userType }}</td>
                                <td>{{ $user->userGroup->name }}</td>

                                <td><span
                                        class="badge bg-label-{{ $user->approved == 1 ? 'success' : 'primary' }} me-1">{{ $user->approved == 1 ? 'True' : 'False' }}</span>
                                </td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn p-0 dropdown-toggle hide-arrow"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            <i class="bx bx-dots-vertical-rounded"></i>
                                        </button>
                                        <div class="dropdown-menu" style="">
                                            <a class="dropdown-item" href="{{ route('staff.edit', $user->id) }}"><i
                                                    class="bx bx-edit-alt me-1"></i>
                                                Edit</a>
                                            {{-- <a class="dropdown-item"
                                                href="{{ route('users.edit-password', $user->id) }}"><i
                                                    class="bx bx-edit-alt me-1"></i>
                                                Edit Password</a> --}}
                                            {{-- <a class="dropdown-item" href="{{ route('users.delete') }}"
                                                onclick="event.preventDefault();
                                    document.getElementById('del-btn').submit();"><i
                                                    class="bx bx-trash me-1"></i>
                                                Delete</a>
                                            <form id="del-btn" action="{{ route('users.delete') }}" method="POST"
                                                class="d-none">
                                                @csrf
                                            </form> --}}
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                @else
                    <p class="text-center">No school users found</p>
                @endif
            </table>
            <br><br><br>

        </div>
    </div>
@endsection
