@extends('layouts.dashboard')

@section('content')
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="card mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="mb-0">Edit Staff</h5>
            <small class="text-muted float-end">Edit Staff</small>
        </div>
        <div class="card-body">
            <form action="{{ route('staff.update', $staff->id) }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">First Name</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" name="firstName" class="form-control" id="basic-icon-default-fullname"
                                    placeholder="John Doe" aria-label="John" aria-describedby="basic-icon-default-fullname2"
                                    value="{{ $staff->firstName }}" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Last Name</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" name="lastName" class="form-control" id="basic-icon-default-fullname"
                                    placeholder="John Doe" aria-label="John" aria-describedby="basic-icon-default-fullname2"
                                    value="{{ $staff->lastName }}" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Middle Name</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" name="middleName" class="form-control"
                                    id="basic-icon-default-fullname" placeholder="John Doe" aria-label="John"
                                    aria-describedby="basic-icon-default-fullname2" value="{{ $staff->middleName }}">
                            </div>
                        </div>

                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">Phone Number</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input type="text" name="phoneNumber" id="basic-icon-default-company"
                                    class="form-control" placeholder="0700568784" aria-label="Doe"
                                    aria-describedby="basic-icon-default-company2" value="{{ $staff->phoneNumber }}"
                                    required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">Phone Number 2</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input type="text" name="phoneNumber2" id="basic-icon-default-company"
                                    class="form-control" placeholder="0700568784" aria-label="Doe"
                                    aria-describedby="basic-icon-default-company2" value="{{ $staff->phoneNumber2 }}">
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Email</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="email" name="email" class="form-control" id="basic-icon-default-fullname"
                                    placeholder="129783" aria-label="Kampala"
                                    aria-describedby="basic-icon-default-fullname2" value="{{ $staff->email }}" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">Birth Date</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <?php $date = date_create($staff->birthDate); ?>
                                <input class="form-control" name="birthDate" type="date"
                                    value="{{ date_format($date, 'Y-m-d') }}" id="datePicker" required>
                            </div>
                        </div>


                    </div>
                    <div class="col-md-6">

                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">Gender</label>
                            <select id="defaultSelect" class="form-select" name="gender" required>
                                <option value="">Please Select Option</option>
                                <option value="MALE" @selected($staff->gender == 'MALE')>MALE</option>
                                <option value="FEMALE" @selected($staff->gender == 'FEMALE')>FEMALE</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">Country</label>
                            <select id="countrySelect" class="form-select" name="countryId" required>
                                <option value="">Select Country</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}" @selected($staff->country->id == $country->id)>{{ $country->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">User Group</label>
                            <select id="countrySelect" class="form-select" name="userGroupId" required>
                                <option value="">Select Group</option>
                                @foreach ($groups as $group)
                                    <option value="{{ $group->id }}" @selected($user->userGroup->id == $group->id)>{{ $group->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">ID TYPE</label>
                            <select id="defaultSelect" class="form-select" name="identificationType" required>
                                <option value="">Please Select Identification</option>
                                <option value="NATIONAL_ID" @selected($staff->identificationType == 'NATIONAL_ID')>NATIONAL ID</option>
                                <option value="DRIVING_PERMIT" @selected($staff->identificationType == 'DRIVING_PERMIT')>DRIVING PERMIT</option>
                                <option value="PASSPORT" @selected($staff->identificationType == 'PASSPORT')>PASSPORT</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">ID Number</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input type="text" name="identificationNumber" id="basic-icon-default-company"
                                    class="form-control" placeholder="Doe" aria-label="Doe"
                                    aria-describedby="basic-icon-default-company2"
                                    value="{{ $staff->identificationNumber }}" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">User Type</label>
                            <select id="defaultSelect" class="form-select" name="userType" required>
                                <option value="">Please Select User Type</option>
                                <option value="DRIVER" @selected($user->userType == 'DRIVER')>DRIVER</option>
                                <option value="SCHOOL_ADMIN" @selected($user->userType == 'SCHOOL_ADMIN')>SCHOOL ADMIN</option>
                                <option value="SCHOOL_STAFF" @selected($user->userType == 'SCHOOL_STAFF')>SCHOOL STAFF</option>
                            </select>
                        </div>
                        <br>
                        <input type="hidden" name="schoolId" value="{{ $user->school->id }}">
                        <button type="submit" class="btn btn-info btn-lg">Edit Staff</button>

                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
