@extends('layouts.dashboard')

@section('content')
    @if (session('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif
    <div class="card mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="mb-0">Search Staff</h5>
            <small class="text-muted float-end">Search Staff</small>
        </div>
        <div class="card-body">
            <form action="{{ route('staff.search-results') }}" method="POST">
                @csrf

                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">School</label>
                            <select id="defaultSelect" class="form-select" name="schoolId" required>
                                <option value="">Select School</option>
                                @foreach ($schools as $school)
                                    <option value="{{ $school->id }}">{{ $school->schoolName }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">User Type</label>
                            <select id="defaultSelect" class="form-select" name="userType" required>
                                <option value="">Select User Type</option>
                                <option value="DRIVER" @selected(old('userType') == 'DRIVER')>DRIVER</option>
                                <option value="SCHOOL_STAFF" @selected(old('userType') == 'SCHOOL_STAFF')>SCHOOL STAFF</option>
                                <option value="SCHOOL_ADMIN" @selected(old('userType') == 'SCHOOL_ADMIN')>SCHOOL ADMIN</option>
                            </select>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div>
    </div>
@endsection
