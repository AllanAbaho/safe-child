@extends('layouts.dashboard')

@section('content')
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif
    @if (session('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {{ session('success') }}
        </div>
    @endif


    <div class="card mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="mb-0">Edit Student</h5>
            <small class="text-muted float-end">Edit Student</small>
        </div>
        <div class="card-body">
            <form action="{{ route('students.update', $id) }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">First Name</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" name="firstName" value="{{ $student->firstName }}"
                                    class="form-control" id="basic-icon-default-fullname" placeholder="Johb"
                                    aria-label="John" aria-describedby="basic-icon-default-fullname2" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">Last Name</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input type="text" name="secondName" value="{{ $student->secondName }}"
                                    id="basic-icon-default-company" class="form-control" placeholder="Doe" aria-label="Doe"
                                    aria-describedby="basic-icon-default-company2" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">School ID Number</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" name="schoolIdNumber" value="{{ $student->schoolIdNumber }}"
                                    class="form-control" id="basic-icon-default-fullname" placeholder="129783"
                                    aria-label="John" aria-describedby="basic-icon-default-fullname2" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Physical Address</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" name="physicalAddress" value="{{ $student->physicalAddress }}"
                                    class="form-control" id="basic-icon-default-fullname"
                                    placeholder="Plot 10 Lukili Avenue" aria-label="John"
                                    aria-describedby="basic-icon-default-fullname2">
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">Email Address</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input type="email" name="email" value="{{ $student->email }}"
                                    id="basic-icon-default-company" class="form-control" placeholder="Doe" aria-label="Doe"
                                    aria-describedby="basic-icon-default-company2">
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">Student Class</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input type="text" name="studentClass" value="{{ $student->studentClass }}"
                                    id="basic-icon-default-company" class="form-control" placeholder="Doe"
                                    aria-label="Doe" aria-describedby="basic-icon-default-company2" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">National ID Number</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" name="nationalIdNumber" value="{{ $student->nationalIdNumber }}"
                                    class="form-control" id="basic-icon-default-fullname" placeholder="129783"
                                    aria-label="John" aria-describedby="basic-icon-default-fullname2">
                            </div>
                        </div>
                        <div class="mb-3">
                            {{-- <img src="{{ asset('codes/345678-fvtgbhrtcdyf.png') }}" alt="Student Code" height="70px"> --}}
                            <img src="{{ asset('codes/' . $student->qrCodeString) }}" alt="Student Code" height="70px">
                        </div>

                        <input type="hidden" name="schoolId" value="{{ $student->school->id }}">
                        <input type="hidden" name="canBeNotified" value="{{ true }}">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Update</button>
            </form>
        </div>
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="mb-0">List of Guardians</h5>
            <a href="{{ route('students.create-guardian', $student->id) }}" class="btn btn-info">Add Guardian</a>
        </div>

        <div class="table-responsive text-nowrap">
            <table class="table">
                <thead>
                    <tr>
                        <th>Full Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                        <th>Relation</th>
                        <th>ID Type</th>
                        <th>ID Number</th>
                        <th>Is Notified</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody class="table-border-bottom-0">
                    @foreach ($guardians as $guardian)
                        <tr>
                            <td><i class="fab fa-angular fa-lg text-danger me-3"></i>
                                <strong>{{ $guardian->guardian->fullName }}</strong>
                            </td>
                            <td>{{ $guardian->guardian->phoneNumber }}</td>
                            <td>{{ $guardian->guardian->address }}</td>
                            <td>{{ $guardian->guardian->relation }}</td>
                            <td> {{ $guardian->guardian->identificationType }}</td>
                            <td> {{ $guardian->guardian->idNumber }}</td>
                            <td><span
                                    class="badge bg-label-{{ $guardian->guardian->isNotified == 1 ? 'success' : 'primary' }} me-1">{{ $guardian->guardian->isNotified == 1 ? 'True' : 'False' }}</span>
                            </td>
                            <td>
                                <div class="dropdown">
                                    <button type="button" class="btn p-0 dropdown-toggle hide-arrow"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                        <i class="bx bx-dots-vertical-rounded"></i>
                                    </button>
                                    <div class="dropdown-menu" style="">
                                        <a class="dropdown-item"
                                            href="{{ route('students.edit-guardian', [$student->id, $guardian->id]) }}"><i
                                                class="bx bx-edit-alt me-1"></i>
                                            Edit</a>
                                        {{-- <a class="dropdown-item" href="{{ route('students.delete') }}"
                                            onclick="event.preventDefault();
                                    document.getElementById('del-btn').submit();"><i
                                                class="bx bx-trash me-1"></i>
                                            Delete</a>
                                        <form id="del-btn" action="{{ route('students.delete') }}" method="POST"
                                            class="d-none">
                                            @csrf
                                        </form> --}}
                                    </div>
                                </div>
                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

    </div>
@endsection
