@extends('layouts.dashboard')

@section('content')
    <div class="card">
        <h5 class="card-header">Student Details</h5>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label" for="basic-icon-default-fullname">First Name</label>
                        <div class="input-group input-group-merge">
                            <input type="text" name="firstName" value="{{ $student->firstName }}" class="form-control"
                                id="basic-icon-default-fullname" placeholder="Johb" aria-label="John"
                                aria-describedby="basic-icon-default-fullname2" readonly>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="basic-icon-default-company">Last Name</label>
                        <div class="input-group input-group-merge">
                            <input type="text" name="secondName" value="{{ $student->secondName }}"
                                id="basic-icon-default-company" class="form-control" placeholder="Doe" aria-label="Doe"
                                aria-describedby="basic-icon-default-company2" readonly>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="basic-icon-default-fullname">School ID Number</label>
                        <div class="input-group input-group-merge">
                            <input type="text" name="schoolIdNumber" value="{{ $student->schoolIdNumber }}"
                                class="form-control" id="basic-icon-default-fullname" placeholder="129783" aria-label="John"
                                aria-describedby="basic-icon-default-fullname2" readonly>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="mb-3">
                        <label class="form-label" for="basic-icon-default-company">Email Address</label>
                        <div class="input-group input-group-merge">
                            <input type="email" name="email" value="{{ $student->email }}"
                                id="basic-icon-default-company" class="form-control" placeholder="Doe" aria-label="Doe"
                                aria-describedby="basic-icon-default-company2" readonly>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="basic-icon-default-company">Student Class</label>
                        <div class="input-group input-group-merge">
                            <input type="text" name="studentClass" value="{{ $student->studentClass }}"
                                id="basic-icon-default-company" class="form-control" placeholder="Doe" aria-label="Doe"
                                aria-describedby="basic-icon-default-company2" readonly>
                        </div>
                    </div>
                    <div class="mb-3">
                        <label class="form-label" for="basic-icon-default-fullname">National ID Number</label>
                        <div class="input-group input-group-merge">
                            <input type="text" name="nationalIdNumber" value="{{ $student->nationalIdNumber }}"
                                class="form-control" id="basic-icon-default-fullname" placeholder="129783" aria-label="John"
                                aria-describedby="basic-icon-default-fullname2" readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <label class="form-label" for="basic-icon-default-fullname">School Name</label>
                <div class="input-group input-group-merge">
                    <input type="text" name="schoolId" value="{{ $student->school->schoolName }}" class="form-control"
                        id="basic-icon-default-fullname" placeholder="129783" aria-label="John"
                        aria-describedby="basic-icon-default-fullname2" readonly>
                </div>

            </div>
        </div>
    </div>
@endsection
