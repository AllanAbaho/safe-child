@extends('layouts.dashboard')

@section('content')
    @if (session('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif
    <div class="card">
        <div class="row">
            <div class="col-md-3">
                <a href="{{ route('students.create', $id) }}" class="btn btn-success mt-3 mx-4"> Add Student</a>
            </div>
        </div>
        <h5 class="card-header">List Of Students</h5>
        <div class="table-responsive text-nowrap container mb-3">
            @if (count($students) > 0)
                <table id="myTable" class="table">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>Class</th>
                            <th>School</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody class="table-border-bottom-0">
                        @foreach ($students as $student)
                            <tr>
                                <td><i class="fab fa-angular fa-lg text-danger me-3"></i>
                                    <strong>{{ $student->firstName }}</strong>
                                </td>
                                <td>{{ $student->secondName }}</td>
                                <td>{{ $student->studentUsername }}</td>
                                <td>{{ $student->email }}</td>
                                <td> {{ $student->studentClass }}</td>
                                <td> {{ $student->school->schoolName }}</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn p-0 dropdown-toggle hide-arrow"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            <i class="bx bx-dots-vertical-rounded"></i>
                                        </button>
                                        <div class="dropdown-menu" style="">
                                            <a class="dropdown-item" href="{{ route('students.edit', $student->id) }}"><i
                                                    class="bx bx-edit-alt me-1"></i>
                                                Edit</a>
                                            <a class="dropdown-item" href="{{ route('students.delete') }}"
                                                onclick="event.preventDefault();
                                    document.getElementById('del-btn').submit();"><i
                                                    class="bx bx-trash me-1"></i>
                                                Delete</a>
                                            <form id="del-btn" action="{{ route('students.delete') }}" method="POST"
                                                class="d-none">
                                                @csrf
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @else
                <p class="text-center">No Students found</p>
            @endif

        </div>
    </div>
@endsection
