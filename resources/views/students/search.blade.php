@extends('layouts.dashboard')

@section('content')
    @if (session('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif
    <div class="card mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="mb-0">Search Student</h5>
            <small class="text-muted float-end">Search Student</small>
        </div>
        <div class="card-body">
            <form action="{{ route('students.search-results') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">School</label>
                            <select id="defaultSelect" class="form-select" name="schoolId" required>
                                <option value="">Select School</option>
                                @foreach ($schools as $school)
                                    <option value="{{ $school->id }}" @selected(old('schoolId') == $school->id)>
                                        {{ $school->schoolName }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">Student ID</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input type="text" name="studentIdNumber" id="basic-icon-default-company"
                                    class="form-control" placeholder="22123" aria-label="Doe"
                                    aria-describedby="basic-icon-default-company2" value="{{ old('studentIdNumber') }}">
                            </div>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Search</button>
            </form>
        </div>
    </div>
@endsection
