@extends('layouts.dashboard')

@section('content')
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="card mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="mb-0">Add Guardian for: {{ $student->firstName . ' ' . $student->secondName }}</h5>
            <small class="text-muted float-end">Add Guardian</small>
        </div>
        <div class="card-body">
            <form action="{{ route('students.guardian') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Full Name</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" name="fullName" class="form-control" id="basic-icon-default-fullname"
                                    placeholder="John Doe" aria-label="John" aria-describedby="basic-icon-default-fullname2"
                                    value="{{ old('fullName') }}" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">Phone Number</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input type="text" name="phoneNumber" id="basic-icon-default-company"
                                    class="form-control" placeholder="0700568784" aria-label="Doe"
                                    aria-describedby="basic-icon-default-company2" value="{{ old('phoneNumber') }}"
                                    required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Address</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" name="address" class="form-control" id="basic-icon-default-fullname"
                                    placeholder="129783" aria-label="Kampala"
                                    aria-describedby="basic-icon-default-fullname2" value="{{ old('address') }}" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">IS NOTIFIED</label>
                            <select id="defaultSelect" class="form-select" name="isNotified" required>
                                <option value="">Please Select Option</option>
                                <option value="FALSE" @selected(old('isNotified') == 'FALSE')>No</option>
                                <option value="TRUE" @selected(old('isNotified') == 'TRUE')>Yes</option>
                            </select>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">ID TYPE</label>
                            <select id="defaultSelect" class="form-select" name="identificationType" required>
                                <option value="">Please Select Identification</option>
                                <option value="NATIONAL_ID" @selected(old('identificationType') == 'NATIONAL_ID')>NATIONAL ID</option>
                                <option value="DRIVING_PERMIT" @selected(old('identificationType') == 'DRIVING_PERMIT')>DRIVING PERMIT</option>
                                <option value="PASSPORT" @selected(old('identificationType') == 'PASSPORT')>PASSPORT</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">ID Number</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input type="text" name="idNumber" id="basic-icon-default-company" class="form-control"
                                    placeholder="Doe" aria-label="Doe" aria-describedby="basic-icon-default-company2"
                                    value="{{ old('idNumber') }}" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">Relation</label>
                            <select id="defaultSelect" class="form-select" name="relation" required>
                                <option value="">Please Select Relationship</option>
                                <option value="MOTHER" @selected(old('identificationType') == 'MOTHER')>MOTHER</option>
                                <option value="FATHER" @selected(old('identificationType') == 'FATHER')>FATHER</option>
                                <option value="BROTHER" @selected(old('identificationType') == 'BROTHER')>BROTHER</option>
                                <option value="SISTER" @selected(old('identificationType') == 'SISTER')>SISTER</option>
                                <option value="UNCLE" @selected(old('identificationType') == 'UNCLE')>UNCLE</option>
                                <option value="AUNT" @selected(old('identificationType') == 'AUNT')>AUNT</option>
                            </select>
                        </div>

                        <input type="hidden" name="studentId" value="{{ $student->id }}">
                        <input type="hidden" name="schoolId" value="{{ $student->school->id }}">
                    </div>
                </div>
                <button type="submit" class="btn btn-info">Add Guardian</button>
            </form>
        </div>
    </div>
@endsection
