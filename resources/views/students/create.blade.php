@extends('layouts.dashboard')

@section('content')
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="card mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="mb-0">Add Student</h5>
            <small class="text-muted float-end">Add Student</small>
        </div>
        <div class="card-body">
            <form action="{{ route('students.store', $id) }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">First Name</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" name="firstName" class="form-control" id="basic-icon-default-fullname"
                                    placeholder="Johb" aria-label="John" aria-describedby="basic-icon-default-fullname2"
                                    value="{{ old('firstName') }}" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">Last Name</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input type="text" name="secondName" id="basic-icon-default-company" class="form-control"
                                    placeholder="Doe" aria-label="Doe" aria-describedby="basic-icon-default-company2"
                                    value="{{ old('secondName') }}" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">School ID Number</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" name="schoolIdNumber" class="form-control"
                                    id="basic-icon-default-fullname" placeholder="129783" aria-label="John"
                                    aria-describedby="basic-icon-default-fullname2" value="{{ old('schoolIdNumber') }}"
                                    required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Physical Address</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" name="physicalAddress" class="form-control"
                                    id="basic-icon-default-fullname" placeholder="Plot 10 Lukili Avenue" aria-label="John"
                                    aria-describedby="basic-icon-default-fullname2" value="{{ old('physicalAddress') }}"
                                    required>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">Email Address</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input type="email" name="email" id="basic-icon-default-company" class="form-control"
                                    placeholder="Doe" aria-label="Doe" aria-describedby="basic-icon-default-company2"
                                    value="{{ old('email') }}">
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-company">Student Class</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-company2" class="input-group-text"><i
                                        class="bx bx-lock"></i></span>
                                <input type="text" name="studentClass" id="basic-icon-default-company"
                                    class="form-control" placeholder="Doe" aria-label="Doe"
                                    aria-describedby="basic-icon-default-company2" value="{{ old('studentClass') }}"
                                    required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">National ID Number</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" name="nationalIdNumber" class="form-control"
                                    id="basic-icon-default-fullname" placeholder="129783" aria-label="John"
                                    aria-describedby="basic-icon-default-fullname2" value="{{ old('nationalIdNumber') }}">
                            </div>
                        </div>
                        <input type="hidden" name="schoolId" value="{{ $id }}">
                        <input type="hidden" name="canBeNotified" value="{{ true }}">
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Send</button>
            </form>
        </div>
    </div>
@endsection
