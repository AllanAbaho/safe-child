@extends('layouts.dashboard')

@section('content')
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="card mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="mb-0">Checking Status...</h5>
            <small class="text-muted float-end">Checking Status</small>
        </div>
        <div class="card-body">
            <form action="{{ route('schools.add-credit') }}" method="POST">
                @csrf
                <div class="row mb-3">
                    <label class="form-label" for="basic-icon-default-fullname">Amount</label>
                    <div class="input-group input-group-merge">
                        <input type="number" class="form-control" name="amount" id="basic-icon-default-fullname"
                            placeholder="10000" aria-label="10000" aria-describedby="basic-icon-default-fullname2"
                            value="{{ $amount }}" readonly>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="form-label" for="basic-icon-default-fullname">Status</label>
                    <div class="input-group input-group-merge">
                        <input type="text" class="form-control" name="status" id="basic-icon-default-fullname"
                            placeholder="" aria-label="10000" aria-describedby="basic-icon-default-fullname2"
                            value="{{ $status }}" readonly>
                    </div>
                </div>

            </form>
        </div>
    </div>
@endsection
<script>
    setInterval(function() {
        url = "{{ route('schools.check-payment-status', $id) }}";
        redirectUrl = "{{ route('schools.dashboard', $schoolId) }}";
        jQuery.post(url, {
                "_token": "{{ csrf_token() }}",
            },
            function(response) {
                console.log(response);
                if (response == 'SUCCESSFUL') {
                    window.location.replace(redirectUrl);
                }
            });
    }, 10000);
</script>
