@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-lg-8 mb-4 order-0">
            <div class="card">
                <div class="d-flex align-items-end row">
                    <div class="col-sm-7">
                        <div class="card-body">
                            <h5 class="card-title text-primary"> {{ $school->schoolName }}! 🎉</h5>
                            <p class="mb-4">
                                Unlock the potential of a streamlined solution for real-time monitoring of students.
                            </p>

                            <a href="javascript:;" class="btn btn-sm btn-outline-primary">View Badges</a>
                        </div>
                    </div>
                    <div class="col-sm-5 text-center text-sm-left">
                        <div class="card-body pb-0 px-0 px-md-4">
                            <img src="{{ asset('assets/img/illustrations/man-with-laptop-light.png') }}" height="140"
                                alt="View Badge User" data-app-dark-img="illustrations/man-with-laptop-dark.png"
                                data-app-light-img="illustrations/man-with-laptop-light.png" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 order-1">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-6 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title d-flex align-items-start justify-content-between">
                                <div class="avatar flex-shrink-0">
                                    <img src="{{ asset('assets/img/icons/unicons/chart-success.png') }}" alt="chart success"
                                        class="rounded" />
                                </div>
                                <div class="dropdown">
                                    <button class="btn p-0" type="button" id="cardOpt3" data-bs-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="bx bx-dots-vertical-rounded"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="cardOpt3">
                                        <a class="dropdown-item"
                                            href="{{ route('staff.index', [$school->id, 'SCHOOL_STAFF']) }}">View More</a>
                                    </div>
                                </div>
                            </div>
                            <span class="fw-semibold d-block mb-1">Staff</span>
                            <h3 class="card-title mb-2">{{ $staff }}</h3>
                            <small class="text-success fw-semibold"><i class="bx bx-up-arrow-alt"></i> +72.80%</small>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-6 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title d-flex align-items-start justify-content-between">
                                <div class="avatar flex-shrink-0">
                                    <img src="{{ asset('assets/img/icons/unicons/wallet-info.png') }}" alt="Credit Card"
                                        class="rounded" />
                                </div>
                                <div class="dropdown">
                                    <button class="btn p-0" type="button" id="cardOpt6" data-bs-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="bx bx-dots-vertical-rounded"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="cardOpt6">
                                        <a class="dropdown-item" href="{{ route('students.index', $school->id) }}">View
                                            More</a>
                                    </div>
                                </div>
                            </div>
                            <span>Students</span>
                            <h3 class="card-title text-nowrap mb-1">{{ $students }}</h3>
                            <small class="text-success fw-semibold"><i class="bx bx-up-arrow-alt"></i> +28.42%</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Total Revenue -->
        <div class="col-12 col-lg-8 order-2 order-md-3 order-lg-2 mb-4">
            <div class="card">
                {{-- <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3989.754742278837!2d32.59076227430581!3d0.32345606401643046!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x177dbb7757ef827b%3A0x396a0898af6720aa!2sPivot%20Payments!5e0!3m2!1sen!2sug!4v1695732619496!5m2!1sen!2sug"
                    height="380" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"
                    style="padding: 10px;"></iframe> --}}
                <div id="map"
                    style="height: 370px;
                width: 100%;
                background-color: grey;"></div>
                <script async defer
                    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAQLRldjFTWHYol_PtdnMv7fen804Y5s7k&callback=drawMap"></script>
            </div>
        </div>
        <!--/ Total Revenue -->
        <div class="col-12 col-md-8 col-lg-4 order-3 order-md-2">
            <div class="row">
                <div class="col-6 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title d-flex align-items-start justify-content-between">
                                <div class="avatar flex-shrink-0">
                                    <img src="{{ asset('assets/img/icons/unicons/paypal.png') }}" alt="Credit Card"
                                        class="rounded" />
                                </div>
                                <div class="dropdown">
                                    <button class="btn p-0" type="button" id="cardOpt4" data-bs-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="bx bx-dots-vertical-rounded"></i>
                                    </button>
                                    <div class="dropdown-menu dropdown-menu-end" aria-labelledby="cardOpt4">
                                        <a class="dropdown-item" href="{{ route('shuttles.index', $school->id) }}">View
                                            More</a>
                                    </div>
                                </div>
                            </div>
                            <span class="d-block mb-1">Shuttles</span>
                            <h3 class="card-title text-nowrap mb-2">{{ $shuttles }}</h3>
                            <small class="text-danger fw-semibold"><i class="bx bx-down-arrow-alt"></i> -14.82%</small>
                        </div>
                    </div>
                </div>
                <div class="col-6 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="card-title d-flex align-items-start justify-content-between">
                                <div class="avatar flex-shrink-0">
                                    <img src="{{ asset('assets/img/icons/unicons/cc-primary.png') }}" alt="Credit Card"
                                        class="rounded" />
                                </div>
                                <div class="dropdown">
                                    <button class="btn p-0" type="button" id="cardOpt1" data-bs-toggle="dropdown"
                                        aria-haspopup="true" aria-expanded="false">
                                        <i class="bx bx-dots-vertical-rounded"></i>
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="cardOpt1">
                                        <a class="dropdown-item"
                                            href="{{ route('staff.index', [$school->id, 'DRIVER']) }}">View More</a>
                                    </div>
                                </div>
                            </div>
                            <span class="fw-semibold d-block mb-1">Drivers</span>
                            <h3 class="card-title mb-2">{{ $drivers }}</h3>
                            <small class="text-success fw-semibold"><i class="bx bx-up-arrow-alt"></i> +28.14%</small>
                        </div>
                    </div>
                </div>

                <div class="col-12 mb-4">
                    <div class="card">
                        <div class="card-body">
                            <div class="d-flex justify-content-between flex-sm-row flex-column gap-3">
                                <div class="d-flex flex-sm-column flex-row align-items-start justify-content-between">
                                    <div class="card-title">
                                        <h5 class="text-nowrap mb-2">Account Balance</h5>
                                        <span class="badge bg-label-warning rounded-pill">Year {{ date('Y') }}</span>
                                    </div>
                                    <div class="mt-sm-auto">
                                        <small class="text-success text-nowrap fw-semibold"><i
                                                class="bx bx-chevron-up"></i> 68.2%</small>
                                        <h4 class="mb-0"><small>UGX</small> {{ $accountBalance }}</h4>
                                    </div>
                                </div>
                                <div id="profileReportChart"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

<script type="text/javascript">
    let locations = [
        ['Mukasa Vincent', 0.3520292286975031, 32.59250294217443],
        ['Alearn Lester', 0.36113487118514354, 32.569962913338166],
        ['Patience kemi', 0.3693684577366103, 32.720640182252126],
        ['Cristiano Ronaldo', 0.4569330247343664, 32.60976251333765],
        ['Granit Xhaka', 0.30387098655247, 32.55219892457992],
    ];

    function initMap() {
        var center = {
            lat: 0.3520292286975031,
            lng: 32.59250294217443
        };
        // var locations = [
        //     ['Mukasa Vincent', 0.3520292286975031, 32.59250294217443],
        //     ['Alearn Lester', 0.36113487118514354, 32.569962913338166],
        //     ['Patience kemi', 0.3693684577366103, 32.720640182252126],
        //     ['Cristiano Ronaldo', 0.4569330247343664, 32.60976251333765],
        //     ['Granit Xhaka', 0.30387098655247, 32.55219892457992],
        // ];
        getLocations();
        async function getLocations() {
            $.ajax({
                url: "{{ route('schools.getSchools') }}",
                type: 'GET',
                dataType: 'json',
                success: function(result) {
                    $.each(result, function(key, value) {
                        var school = [value.schoolName, value.location.latitude, value
                            .location
                            .longitude
                        ];
                        locations.push(school);

                    });
                    console.log(locations);
                    drawMap();
                }
            });
        }

        // function drawMap() {
        //     var map = new google.maps.Map(document.getElementById('map'), {
        //         zoom: 11,
        //         center: center
        //     });
        //     var infowindow = new google.maps.InfoWindow({});
        //     var marker, count;
        //     marker = new google.maps.Marker({
        //         position: new google.maps.LatLng(0.3516353634415819, 32.63353583068644),
        //         map: map,
        //         title: 'Kyambogo College School',
        //         // icon: "{{ asset('assets/img/school.png') }}"
        //     });
        //     for (count = 0; count < locations.length; count++) {
        //         marker = new google.maps.Marker({
        //             position: new google.maps.LatLng(locations[count][1], locations[count][2]),
        //             map: map,
        //             title: locations[count][0],
        //             icon: "{{ asset('assets/img/user (1).png') }}"
        //         });
        //         google.maps.event.addListener(marker, 'click', (function(marker, count) {

        //             return function() {
        //                 infowindow.setContent(locations[count][0]);
        //                 infowindow.open(map, marker);
        //             }
        //         })(marker, count));
        //     }
        // }
    }
    let directionsService, directionsRenderer;

    function drawMap() {
        var center = {
            lat: 0.3520292286975031,
            lng: 32.59250294217443
        };
        directionsService = new google.maps.DirectionsService();
        directionsRenderer = new google.maps.DirectionsRenderer();

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 11,
            center: center
        });
        google.maps.event.addListener(map, "click", function(event) {
            this.setOptions({
                scrollwheel: true,
                icon: "{{ asset('assets/img/user (1).png') }}"

            });
        })

        directionsRenderer.setMap(map);
        calculateRoute()
    }

    function calculateRoute() {
        var stops = []
        for (i in locations) {
            stops.push({
                location: new google.maps.LatLng(locations[i][1], locations[i][2]),
                stopover: true
            });
        }
        var request = {
            origin: new google.maps.LatLng(0.3520292286975031, 32.59250294217443),
            destination: new google.maps.LatLng(0.30387098655247, 32.55219892457992),
            waypoints: stops,
            optimizeWaypoints: true,
            travelMode: google.maps.DirectionsTravelMode.DRIVING
        };

        directionsService.route(request, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsRenderer.setDirections(response);
            }
        });
    }
</script>
