@extends('layouts.dashboard')

@section('content')
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="card mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="mb-0">Edit School</h5>
            <small class="text-muted float-end">Edit School</small>
        </div>
        <div class="card-body">
            <form action="{{ route('schools.update', $id) }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">School Name</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" class="form-control" name="schoolName"
                                    value="{{ $school->schoolName }}" id="basic-icon-default-fullname"
                                    placeholder="John Doe" aria-label="John Doe"
                                    aria-describedby="basic-icon-default-fullname2" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">School Email</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" class="form-control" name="email" value="{{ $school->email }}"
                                    id="basic-icon-default-fullname" placeholder="John Doe" aria-label="John Doe"
                                    aria-describedby="basic-icon-default-fullname2" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Phone Number</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" class="form-control" name="phoneNumber"
                                    value="{{ $school->phoneNumber }}" id="basic-icon-default-fullname"
                                    placeholder="John Doe" aria-label="John Doe"
                                    aria-describedby="basic-icon-default-fullname2" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">SMS Cost</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" class="form-control" name="smsCost" value="{{ $school->smsCost }}"
                                    id="basic-icon-default-fullname" placeholder="John Doe" aria-label="John Doe"
                                    aria-describedby="basic-icon-default-fullname2" required>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Physical Address</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" class="form-control" name="physicalAddress"
                                    id="basic-icon-default-fullname" value="{{ $school->address }}" placeholder="Kalerwe"
                                    aria-label="Kalerwe" aria-describedby="basic-icon-default-fullname2" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">School Category</label>
                            <select id="defaultSelect" class="form-select" name="schoolCategory" required>
                                <option value="{{ $school->schoolCategory }}">{{ $school->schoolCategory }}</option>
                                <option value="DAY_CARE">DAY CARE</option>
                                <option value="PRIMARY">PRIMARY</option>
                                <option value="SECONDARY">SECONDARY</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Latitude</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" class="form-control" name="latitudeCoordinate"
                                    value="{{ $school->location->latitude }}" id="basic-icon-default-fullname"
                                    placeholder="John Doe" aria-label="John Doe"
                                    aria-describedby="basic-icon-default-fullname2" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Longititude</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" class="form-control" name="longitudeCoordinate"
                                    value="{{ $school->location->longitude }}" id="basic-icon-default-fullname"
                                    placeholder="John Doe" aria-label="John Doe"
                                    aria-describedby="basic-icon-default-fullname2" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">Select Country</label>
                            <select id="countrySelect" class="form-select" name="countryId" required>
                                <option value="{{ $school->country->id }}">{{ $school->country->name }}</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">Select District</label>
                            <select id="districtSelect" class="form-select" name="districtId" required>
                                <option value="{{ $school->district->id }}">{{ $school->district->name }}</option>
                            </select>
                        </div>
                    </div>
                </div>


                <button type="submit" class="btn btn-primary">Send</button>
            </form>
        </div>
    </div>
@endsection
<script type="text/javascript">
    window.addEventListener('load', function() {
        $('#countrySelect').on('change', function() {
            var countryId = this.value;
            if (countryId) {
                $.ajax({
                    url: '/getDistricts/' + countryId,
                    type: 'GET',
                    dataType: 'json',
                    success: function(result) {
                        if (result.length > 0) {
                            $.each(result, function(key, value) {
                                console.log(value.name);
                                $("#districtSelect").append('<option value="' +
                                    value
                                    .id + '">' + value.name + '</option>');
                            });
                        } else {
                            $("#districtSelect").empty();
                        }
                    }
                });

            } else {}
        });
    });
</script>
