@extends('layouts.dashboard')

@section('content')
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="card mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="mb-0">Add Credit</h5>
            <small class="text-muted float-end">Add Credit</small>
        </div>
        <div class="card-body">
            <form action="{{ route('schools.add-credit') }}" method="POST">
                @csrf
                <div class="row mb-3">
                    <label class="form-label" for="basic-icon-default-fullname">Amount</label>
                    <div class="input-group input-group-merge">
                        <span id="basic-icon-default-fullname2" class="input-group-text"><i class="bx bx-user"></i></span>
                        <input type="number" class="form-control" name="amount" id="basic-icon-default-fullname"
                            placeholder="10000" aria-label="10000" aria-describedby="basic-icon-default-fullname2"
                            value="{{ old('amount') }}" required>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Send</button>
            </form>
        </div>
    </div>
@endsection
