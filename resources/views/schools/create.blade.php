@extends('layouts.dashboard')

@section('content')
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="card mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="mb-0">Add School</h5>
            <small class="text-muted float-end">Add School</small>
        </div>
        <div class="card-body">
            <form action="{{ route('schools.store') }}" method="POST">
                @csrf
                <div class="row">
                    <div class="col-md-6">
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">School Name</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" class="form-control" name="schoolName"
                                    id="basic-icon-default-fullname" placeholder="John Doe" aria-label="John Doe"
                                    aria-describedby="basic-icon-default-fullname2" value="{{ old('schoolName') }}"
                                    required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">School Email</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="email" class="form-control" name="email" id="basic-icon-default-fullname"
                                    placeholder="John Doe" aria-label="John Doe"
                                    aria-describedby="basic-icon-default-fullname2" value="{{ old('email') }}" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Phone Number</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" class="form-control" name="phoneNumber"
                                    id="basic-icon-default-fullname" placeholder="John Doe" aria-label="John Doe"
                                    aria-describedby="basic-icon-default-fullname2" value="{{ old('phoneNumber') }}"
                                    required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">SMS Cost</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="number" class="form-control" name="smsCost" id="basic-icon-default-fullname"
                                    placeholder="John Doe" aria-label="John Doe"
                                    aria-describedby="basic-icon-default-fullname2" value="{{ old('smsCost') }}" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Physical Address</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" class="form-control" name="physicalAddress"
                                    id="basic-icon-default-fullname" placeholder="Kalerwe" aria-label="Kalerwe"
                                    aria-describedby="basic-icon-default-fullname2" value="{{ old('physicalAddress') }}"
                                    required>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-6">
                        {{-- <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">School Username</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="text" class="form-control" name="username" id="basic-icon-default-fullname"
                                    placeholder="Username" aria-label="John Doe"
                                    aria-describedby="basic-icon-default-fullname2" required>
                            </div>
                        </div> --}}
                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">School Category</label>
                            <select id="defaultSelect" class="form-select" name="schoolCategory" required>
                                <option value="">Select Category</option>
                                <option value="DAY_CARE" @selected(old('schoolCategory') == 'DAY_CARE')>DAY CARE</option>
                                <option value="PRIMARY" @selected(old('schoolCategory') == 'PRIMARY')>PRIMARY</option>
                                <option value="SECONDARY" @selected(old('schoolCategory') == 'SECONDARY')>SECONDARY</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Latitude</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="number" class="form-control" name="latitudeCoordinate"
                                    id="basic-icon-default-fullname" placeholder="John Doe" aria-label="John Doe"
                                    aria-describedby="basic-icon-default-fullname2"
                                    value="{{ old('latitudeCoordinate') }}" step="any" required>
                            </div>
                        </div>

                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-fullname">Longitude</label>
                            <div class="input-group input-group-merge">
                                <span id="basic-icon-default-fullname2" class="input-group-text"><i
                                        class="bx bx-user"></i></span>
                                <input type="number" class="form-control" name="longitudeCoordinate"
                                    id="basic-icon-default-fullname" placeholder="John Doe" aria-label="John Doe"
                                    aria-describedby="basic-icon-default-fullname2"
                                    value="{{ old('longitudeCoordinate') }}" step="any" required>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">Select Country</label>
                            <select id="countrySelect" class="form-select" name="countryId" required>
                                <option value="">Select Country</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->id }}" @selected(old('countryId') == $country->id)>{{ $country->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label for="defaultSelect" class="form-label">Select District</label>
                            <select id="districtSelect" class="form-select" name="districtId" required>
                            </select>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Send</button>
            </form>
        </div>
    </div>
@endsection

<script type="text/javascript">
    window.addEventListener('load', function() {
        var countrySelect = $('#countrySelect').find(":selected").val();

        $.ajax({
            url: '/getDistricts/' + countrySelect,
            type: 'GET',
            dataType: 'json',
            success: function(result) {
                if (result.length > 0) {
                    $.each(result, function(key, value) {
                        $("#districtSelect").append('<option value="' +
                            value
                            .id + '">' + value.name + '</option>');
                    });
                } else {
                    $("#districtSelect").empty();
                }
            }
        });
        $('#countrySelect').on('change', function() {
            var countryId = this.value;
            var districtsUrl = "{{ route('schools.getDistricts', '') }}" + "/" + countryId;
            if (countryId) {
                $.ajax({
                    url: districtsUrl,
                    type: 'GET',
                    dataType: 'json',
                    success: function(result) {
                        if (result.length > 0) {
                            $.each(result, function(key, value) {
                                $("#districtSelect").append('<option value="' +
                                    value
                                    .id + '">' + value.name + '</option>');
                            });
                        } else {
                            $("#districtSelect").empty();
                        }
                    }
                });

            } else {}
        });
    });
</script>
