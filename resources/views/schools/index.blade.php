@extends('layouts.dashboard')

@section('content')
    @if (session('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif
    <div class="card">
        <div class="row">
            <div class="col-md-3">
                <a href="{{ route('schools.create') }}" class="btn btn-success mt-3 mx-4"> Add School</a>
            </div>
        </div>
        <h5 class="card-header">List Of Schools</h5>
        <div class="table-responsive text-nowrap container mb-3">
            <table id="myTable" class="table">
                <thead>
                    <tr>
                        <th>School Name</th>
                        <th>School Email</th>
                        <th>Phone Number</th>
                        <th>SMS Cost</th>
                        <th>Category</th>
                        <th>District</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody class="table-border-bottom-0">
                    @foreach ($schools as $school)
                        <tr>
                            <td><i class="fab fa-angular fa-lg text-danger me-3"></i>
                                <a href="{{ route('schools.dashboard', $school->id) }}">{{ $school->schoolName }}</a>

                            </td>
                            <td>{{ $school->email }}</td>
                            <td>{{ $school->phoneNumber }}</td>
                            <td>{{ $school->smsCost }}</td>
                            <td>{{ $school->schoolCategory }}</td>
                            <td>{{ $school->district->name }}</td>

                            <td>
                                <div class="dropdown">
                                    <button type="button" class="btn p-0 dropdown-toggle hide-arrow"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                        <i class="bx bx-dots-vertical-rounded"></i>
                                    </button>
                                    <div class="dropdown-menu" style="">
                                        <a class="dropdown-item" href="{{ route('schools.edit', $school->id) }}"><i
                                                class="bx bx-edit-alt me-1"></i>
                                            Edit</a>
                                        <a class="dropdown-item" href="{{ route('schools.delete') }}"
                                            onclick="event.preventDefault();
                                    document.getElementById('del-btn').submit();"><i
                                                class="bx bx-trash me-1"></i>
                                            Delete</a>
                                        <form id="del-btn" action="{{ route('schools.delete') }}" method="POST"
                                            class="d-none">
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
