@extends('layouts.dashboard')

@section('content')
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="card mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="mb-0">Edit Shuttle</h5>
            <small class="text-muted float-end">Edit Shuttle</small>
        </div>
        <div class="card-body">
            <form action="{{ route('shuttles.update', $shuttle->id) }}" method="POST">
                @csrf
                <div class="mb-3">
                    <label class="form-label" for="basic-icon-default-fullname">Plate Number</label>
                    <div class="input-group input-group-merge">
                        <span id="basic-icon-default-fullname2" class="input-group-text"><i class="bx bx-user"></i></span>
                        <input type="text" name="plateNumber" class="form-control" id="basic-icon-default-fullname"
                            placeholder="jDoe" aria-label="John Doe" aria-describedby="basic-icon-default-fullname2"
                            value="{{ $shuttle->plateNumber }}" required>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="basic-icon-default-company">Vehicle Model</label>
                    <div class="input-group input-group-merge">
                        <span id="basic-icon-default-company2" class="input-group-text"><i class="bx bx-lock"></i></span>
                        <input type="text" name="vehicleModel" id="basic-icon-default-company" class="form-control"
                            placeholder="Noah" aria-label="ACME Inc." aria-describedby="basic-icon-default-company2"
                            value="{{ $shuttle->vehicleModel }}" required>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="basic-icon-default-company">Maximum Capacity</label>
                    <div class="input-group input-group-merge">
                        <span id="basic-icon-default-company2" class="input-group-text"><i class="bx bx-lock"></i></span>
                        <input type="number" name="maximumCapacity" id="basic-icon-default-company" class="form-control"
                            placeholder="Noah" aria-label="ACME Inc." aria-describedby="basic-icon-default-company2"
                            value="{{ $shuttle->maximumCapacity }}" required>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="basic-icon-default-company">Current Driver</label>
                    <select id="countrySelect" class="form-select" name="currentDriverId" required>
                        <option value="">Select Driver</option>
                        @foreach ($drivers as $driver)
                            <option value="{{ $driver->id }}" @selected($shuttle->currentDriver->id == $driver->id)>
                                {{ $driver->username }}
                            </option>
                        @endforeach
                    </select>
                </div>
                <input type="hidden" name="schoolId" value="{{ $id }}">
                <button type="submit" class="btn btn-primary">Send</button>
            </form>
        </div>
    </div>
@endsection
