@extends('layouts.dashboard')

@section('content')
    @if (session('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif
    <div class="card">
        <div class="row">
            <div class="col-md-3">
                <a href="{{ route('shuttles.create', $id) }}" class="btn btn-success mt-3 mx-4"> Add Shuttle</a>
            </div>
        </div>
        <h5 class="card-header">List Of Shuttles</h5>
        <div class="table-responsive text-nowrap container mb-3">
            <table id="myTable" class="table">
                @if (count($shuttles) > 0)
                    <thead>
                        <tr>
                            <th>Plate Number</th>
                            <th>Vehicle Model</th>
                            <th>Maximum Capacity</th>
                            <th>Current Driver</th>
                            <th>School</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody class="table-border-bottom-0">
                        @foreach ($shuttles as $shuttle)
                            <tr>
                                <td><i class="fab fa-angular fa-lg text-danger me-3"></i>
                                    <strong>{{ $shuttle->plateNumber }}</strong>
                                </td>
                                <td>{{ $shuttle->vehicleModel }}</td>
                                <td>{{ $shuttle->maximumCapacity }}</td>
                                <td> {{ $shuttle->currentDriver->username }}</td>
                                <td> {{ $shuttle->school->schoolName }}</td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn p-0 dropdown-toggle hide-arrow"
                                            data-bs-toggle="dropdown" aria-expanded="false">
                                            <i class="bx bx-dots-vertical-rounded"></i>
                                        </button>
                                        <div class="dropdown-menu" style="">
                                            <a class="dropdown-item" href="{{ route('shuttles.edit', $shuttle->id) }}"><i
                                                    class="bx bx-edit-alt me-1"></i>
                                                Edit</a>
                                            <a class="dropdown-item" href="{{ route('shuttles.delete') }}"
                                                onclick="event.preventDefault();
                                    document.getElementById('del-btn').submit();"><i
                                                    class="bx bx-trash me-1"></i>
                                                Delete</a>
                                            <form id="del-btn" action="{{ route('shuttles.delete') }}" method="POST"
                                                class="d-none">
                                                @csrf
                                            </form>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <p class="text-center">No shuttles found</p>
                    </tbody>
                @endif

            </table>
            <br><br>
        </div>
    </div>
@endsection
