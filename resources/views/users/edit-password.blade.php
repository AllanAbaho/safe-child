@extends('layouts.dashboard')

@section('content')
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="card mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="mb-0">Change Password</h5>
            <small class="text-muted float-end">Change Password</small>
        </div>
        <div class="card-body">
            <form action="{{ route('users.update-password') }}" method="POST">
                @csrf

                <div class="mb-3">
                    <label class="form-label" for="basic-icon-default-fullname">Old Password </label>
                    <div class="input-group input-group-merge">
                        <span id="basic-icon-default-fullname2" class="input-group-text"><i class="bx bx-user"></i></span>
                        <input type="password" name="oldPassword" value="{{ old('oldPassword') }}" class="form-control"
                            id="basic-icon-default-fullname" placeholder="John Doe" aria-label="John"
                            aria-describedby="basic-icon-default-fullname2" required>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="basic-icon-default-fullname">New Password </label>
                    <div class="input-group input-group-merge">
                        <span id="basic-icon-default-fullname2" class="input-group-text"><i class="bx bx-user"></i></span>
                        <input type="password" name="newPassword" value="{{ old('newPassword') }}" class="form-control"
                            id="basic-icon-default-fullname" placeholder="John Doe" aria-label="John"
                            aria-describedby="basic-icon-default-fullname2" required>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="basic-icon-default-fullname">Confirm Password</label>
                    <div class="input-group input-group-merge">
                        <span id="basic-icon-default-fullname2" class="input-group-text"><i class="bx bx-user"></i></span>
                        <input type="password" name="confirmPassword" value="{{ old('confirmPassword') }}"
                            class="form-control" id="basic-icon-default-fullname" placeholder="John Doe" aria-label="John"
                            aria-describedby="basic-icon-default-fullname2" required>
                    </div>
                </div>

                <button type="submit" class="btn btn-info">Update</button>

        </div>
        </form>
    </div>
    </div>
@endsection
