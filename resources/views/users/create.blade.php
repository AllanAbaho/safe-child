@extends('layouts.dashboard')

@section('content')
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="card mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="mb-0">Add User</h5>
            <small class="text-muted float-end">Add User</small>
        </div>
        <div class="card-body">
            <form action="{{ route('users.store') }}" method="POST">
                @csrf
                <div class="mb-3">
                    <label class="form-label" for="basic-icon-default-fullname">Username</label>
                    <div class="input-group input-group-merge">
                        <span id="basic-icon-default-fullname2" class="input-group-text"><i class="bx bx-user"></i></span>
                        <input type="text" name="username" class="form-control" id="basic-icon-default-fullname"
                            placeholder="jDoe" aria-label="John Doe" aria-describedby="basic-icon-default-fullname2"
                            value="{{ old('username') }}" required>
                    </div>
                </div>
                <div class="mb-3">
                    <label class="form-label" for="basic-icon-default-company">Password</label>
                    <div class="input-group input-group-merge">
                        <span id="basic-icon-default-company2" class="input-group-text"><i class="bx bx-lock"></i></span>
                        <input type="password" name="password" id="basic-icon-default-company" class="form-control"
                            placeholder="password" aria-label="ACME Inc." aria-describedby="basic-icon-default-company2"
                            value="{{ old('password') }}" required>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="defaultSelect" class="form-label">User Group</label>
                    <select id="groupSelect" class="form-select" name="userGroupId">
                        @foreach ($groups as $group)
                            <option value="{{ $group->id }} @selected(old('userGroupId') == $group->id)">{{ $group->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-3">
                    <label for="defaultSelect" class="form-label">User Type</label>
                    <select id="typeSelect" class="form-select" name="userType">
                        <option value="ADMIN" @selected(old('userType') == 'ADMIN')>ADMIN</option>
                        <option value="SCHOOL_ADMIN" @selected(old('userType') == 'SCHOOL_ADMIN')>SCHOOL ADMIN</option>
                        <option value="SCHOOL_STAFF" @selected(old('userType') == 'SCHOOL_STAFF')>SCHOOL_STAFF</option>
                        <option value="DRIVER @selected(old('userType') == 'DRIVER')">DRIVER</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Send</button>
            </form>
        </div>
    </div>
@endsection
