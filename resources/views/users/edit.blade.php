@extends('layouts.dashboard')

@section('content')
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif

    <div class="card mb-4">
        <div class="card-header d-flex justify-content-between align-items-center">
            <h5 class="mb-0">Edit User</h5>
            <small class="text-muted float-end">Edit User</small>
        </div>
        <div class="card-body">
            <form action="{{ route('users.update', $id) }}" method="POST">
                @csrf
                <div class="mb-3">
                    <label class="form-label" for="basic-icon-default-fullname">Username</label>
                    <div class="input-group input-group-merge">
                        <span id="basic-icon-default-fullname2" class="input-group-text"><i class="bx bx-user"></i></span>
                        <input type="text" class="form-control" name="username" value="{{ $user->username }}"
                            id="basic-icon-default-fullname" placeholder="John Doe" aria-label="John Doe"
                            aria-describedby="basic-icon-default-fullname2" required>
                    </div>
                </div>
                <div class="mb-3">
                    <label for="defaultSelect" class="form-label">User Group</label>
                    <select id="groupSelect" class="form-select" name="userGroupId">
                        <option value="{{ $user->userGroup->id }}">{{ $user->userGroup->name }}</option>
                    </select>
                </div>
                <div class="mb-3">
                    <label for="defaultSelect" class="form-label">User Type</label>
                    <select id="typeSelect" class="form-select" name="userType">
                        <option value={{ $user->userType }}>{{ $user->userType }}</option>
                    </select>
                </div>
                <div class="form-check form-switch mb-3">
                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckChecked" name="approved"
                        {{ $user->approved == true ? 'checked' : '' }}>
                    <label class="form-check-label" for="flexSwitchCheckChecked">Approved</label>
                </div>

                <button type="submit" class="btn btn-primary">Send</button>
            </form>
        </div>
    </div>
@endsection
{{-- <script type="text/javascript">
    window.addEventListener('load', function() {
        $("#flexSwitchCheckChecked").prop('checked', true);
    });
</script> --}}
