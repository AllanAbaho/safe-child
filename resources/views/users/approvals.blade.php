@extends('layouts.dashboard')

@section('content')
    @if (session('success'))
        <div class="alert alert-success alert-dismissible" role="alert">
            {{ session('success') }}
        </div>
    @endif
    @if (session('error'))
        <div class="alert alert-danger alert-dismissible" role="alert">
            {{ session('error') }}
        </div>
    @endif
    <div class="card">
        <h5 class="card-header">List Of Approvals</h5>
        <div class="table-responsive text-nowrap">
            <table class="table">
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>School Name</th>
                        <th>User Type</th>
                        <th>User Group</th>
                        <th>Status</th>
                        {{-- <th>Modified Date</th>
                        <th>Modified By</th> --}}
                    </tr>
                </thead>
                <tbody class="table-border-bottom-0">
                    @foreach ($users as $user)
                        <tr>
                            <td><i class="fab fa-angular fa-lg text-danger me-3"></i>
                                <strong>{{ $user->user->username }}</strong>
                            </td>
                            <td>{{ $user->school->schoolName }}</td>
                            <td>{{ $user->user->userType }}</td>
                            <td>{{ $user->user->userGroup->name }}</td>

                            <td><span
                                    class="badge bg-label-{{ $user->status == 'APPROVED' ? 'success' : 'primary' }} me-1">{{ $user->status }}</span>
                            </td>
                            {{-- <td>{{ $user->modifiedOn }}</td>
                            <td>{{ $user->modifiedBy->username }}</td> --}}

                            <td>
                                <div class="dropdown">
                                    <button type="button" class="btn p-0 dropdown-toggle hide-arrow"
                                        data-bs-toggle="dropdown" aria-expanded="false">
                                        <i class="bx bx-dots-vertical-rounded"></i>
                                    </button>
                                    <div class="dropdown-menu" style="">
                                        <a class="dropdown-item"
                                            href="{{ route('users.approve', [$user->id, $user->user->id]) }}"
                                            onclick="event.preventDefault();
                                    document.getElementById('del-btn').submit();"><i
                                                class="bx bx-trash me-1"></i>
                                            Approve</a>
                                        <form id="del-btn"
                                            action="{{ route('users.approve', [$user->id, $user->user->id]) }}"
                                            method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
